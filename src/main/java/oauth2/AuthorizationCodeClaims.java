package oauth2;

public class AuthorizationCodeClaims {
	public static final String CLIENT_ID = AuthorizeEndpoint.REQUEST_PARAMETER_CLIENT_ID;
	public static final String REDIRECT_URI = AuthorizeEndpoint.REQUEST_PARAMETER_REDIRECT_URI;
	public static final String SCOPE = AuthorizeEndpoint.REQUEST_PARAMETER_SCOPE;
	public static final String CODE_CHALLENGE = AuthorizeEndpoint.PKCE_REQUEST_PARAMETER_CODE_CHALLENGE;
	public static final String CODE_CHALLENGE_METHOD = AuthorizeEndpoint.PKCE_REQUEST_PARAMETER_CODE_CHALLENGE_METHOD;
	public static final String NONCE = AuthorizeEndpoint.OPENID_REQUEST_PARAMETER_NONCE;
}
