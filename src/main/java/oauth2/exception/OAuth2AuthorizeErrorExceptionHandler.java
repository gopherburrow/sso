package oauth2.exception;

import java.util.Arrays;

import freud.err.ExceptionTranslation;
import freud.msg.I18n;
import freud.web.UrlEditor;
import oauth2.AuthorizeEndpoint;
import spark.Request;
import spark.Response;

public class OAuth2AuthorizeErrorExceptionHandler {

	private static final String ERR_NEW_UNSUPPORTED = "microsvc.exception.OAuth2ErrorExceptionHandler.new.unsupported";

	private OAuth2AuthorizeErrorExceptionHandler() {
		throw new UnsupportedOperationException(ERR_NEW_UNSUPPORTED);
	}
	
	public static void handle(OAuth2AuthorizeErrorException e, Request req, Response resp) {
		UrlEditor redirectUriEditor = new UrlEditor(e.redirectUri);
		redirectUriEditor.setQueryParameterValues(AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR, e.error);
		if (e.errorDescription != null) {
			redirectUriEditor.setQueryParameterValues(AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_DESCRIPTION, e.errorDescription);
		}
		if (e.errorUri != null) {
			redirectUriEditor.setQueryParameterValues(AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_URI, e.errorUri);
		}
		if (e.state != null) {
			redirectUriEditor.setQueryParameterValues(AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_STATE, e.state);
		}
		resp.redirect(redirectUriEditor.toUrlString());
	}

	public static ExceptionTranslation toOAuth2AuthorizeError(Enum<?> enumValue, String error) {
		return ExceptionTranslation.byEnum(enumValue, e->{
			if(e.extraInfo.length < 2) {
				new IllegalArgumentException("EnumException extraInfo must have at least 2 parameters (redirectUri and state).");
			}
			var eil = e.extraInfo.length - 2;
			var extraInfo = new Object[eil]; 
			System.arraycopy(e.extraInfo, 0, extraInfo, 0, eil);
			var implicitRedirectUri = e.extraInfo[eil];
			var state = e.extraInfo[eil + 1];
			return new OAuth2AuthorizeErrorException(implicitRedirectUri, error, I18n.getString(e.getMessage(), extraInfo), null, state, e);
		});
	}

}
