package oauth2.exception;

public class OAuth2AuthorizeErrorException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public String redirectUri;
	public String error;
	public String errorDescription;
	public String errorUri;
	public String state;
	public OAuth2AuthorizeErrorException(String redirectUri, String error, String errorDescription, String errorUri, String state, Throwable cause) {
		super(errorDescription, cause, true, false);
		this.redirectUri = redirectUri;
		this.error = error;
		this.errorDescription = errorDescription;
		this.errorUri = errorUri;
		this.state = state;
	}
}
