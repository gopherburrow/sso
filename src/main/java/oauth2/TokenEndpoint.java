package oauth2;

public class TokenEndpoint {
	public static final String REQUEST_FORM_PARAMETER_GRANT_TYPE = "grant_type";
	public static final String REQUEST_FORM_PARAMETER_CODE = AuthorizeEndpoint.RESPONSE_PARAMETER_CODE;
	public static final String REQUEST_FORM_PARAMETER_REDIRECT_URI = AuthorizeEndpoint.REQUEST_PARAMETER_REDIRECT_URI;
	public static final String REQUEST_FORM_PARAMETER_CLIENT_ID = AuthorizeEndpoint.REQUEST_PARAMETER_CLIENT_ID;

	public static final String REQUEST_FORM_PARAMETER_GRANT_TYPE_AUTHORIZATION_CODE = "authorization_code";
	public static final String REQUEST_FORM_PARAMETER_GRANT_TYPE_PASSWORD = "password";
	public static final String REQUEST_FORM_PARAMETER_GRANT_TYPE_CLIENT_CREDENTIALS = "client_credentials";
	public static final String REQUEST_FORM_PARAMETER_CODE_VERIFIER = "code_verifier";
}
