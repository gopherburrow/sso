package oauth2;

public class AuthorizeEndpoint {

	//OAuth2 authorization request parameters defined in RFC 6749 (https://tools.ietf.org/html/rfc6749)
	public final static String REQUEST_PARAMETER_RESPONSE_TYPE = "response_type";
	public final static String REQUEST_PARAMETER_CLIENT_ID = "client_id";
	public final static String REQUEST_PARAMETER_REDIRECT_URI = "redirect_uri";
	public final static String REQUEST_PARAMETER_SCOPE = "scope";
	public final static String REQUEST_PARAMETER_STATE = "state";

	//OAuth2 authorization request parameters values defined in RFC 6749
	public final static String REQUEST_PARAMETER_RESPONSE_TYPE_CODE = "code";
	public final static String REQUEST_PARAMETER_RESPONSE_TYPE_TOKEN = "token";

	//OAuth2 authorization response parameters defined in RFC 6749
	public final static String RESPONSE_PARAMETER_CODE = "code";
	public final static String RESPONSE_PARAMETER_STATE = REQUEST_PARAMETER_STATE;

	//OAuth2 authorization error response parameters defined in RFC 6749
	public final static String ERROR_RESPONSE_PARAMETER_ERROR = "error";
	public final static String ERROR_RESPONSE_PARAMETER_ERROR_DESCRIPTION = "error_description";
	public final static String ERROR_RESPONSE_PARAMETER_ERROR_URI = "error_uri";
	public final static String ERROR_RESPONSE_PARAMETER_STATE = REQUEST_PARAMETER_STATE;

	//OAuth2 authorization error response parameters values defined in RFC 6749
	public final static String ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST = "invalid_request";
	public final static String ERROR_RESPONSE_PARAMETER_ERROR_UNAUTHORIZED_CLIENT = "unauthorized_client";
	public final static String ERROR_RESPONSE_PARAMETER_ERROR_ACCESS_DENIED = "access_denied";
	public final static String ERROR_RESPONSE_PARAMETER_ERROR_UNSUPPORTED_RESPONSE_TYPE = "unsupported_response_type";
	public final static String ERROR_RESPONSE_PARAMETER_ERROR_INVALID_SCOPE = "invalid_scope";
	public final static String ERROR_RESPONSE_PARAMETER_ERROR_SERVER_ERROR = "server_error";
	public final static String ERROR_RESPONSE_PARAMETER_ERROR_TEMPORARILY_UNAVAILABLE = "temporarily_unavailable";

	//OAuth2 PKCE (RFC7636) request parameter extensions  defined in RFC 7636 (https://tools.ietf.org/html/rfc7636)
	public final static String PKCE_REQUEST_PARAMETER_CODE_CHALLENGE = "code_challenge";
	public final static String PKCE_REQUEST_PARAMETER_CODE_CHALLENGE_METHOD = "code_challenge_method";

	//OAuth2 PKCE (RFC7636) request parameter extensions values defined in RFC 7636
	public final static String PKCE_REQUEST_PARAMETER_CODE_CHALLENGE_METHOD_PLAIN = "plain";
	public final static String PKCE_REQUEST_PARAMETER_CODE_CHALLENGE_METHOD_S256 = "S256";
	
	//OpenID Connect request parameter extensions (https://openid.net/specs/openid-connect-core-1_0.html)
	public final static String OPENID_REQUEST_PARAMETER_NONCE = "nonce";
	public final static String OPENID_REQUEST_PARAMETER_DISPLAY = "display";
	public final static String OPENID_REQUEST_PARAMETER_PROMPT = "prompt";
	public final static String OPENID_REQUEST_PARAMETER_MAX_AGE = "max_age";
	public final static String OPENID_REQUEST_PARAMETER_UI_LOCALES = "ui_locales";
	public final static String OPENID_REQUEST_PARAMETER_ID_TOKEN_HINT = "id_token_hint";
	public final static String OPENID_REQUEST_PARAMETER_LOGIN_HINT = "login_hint";
	public final static String OPENID_REQUEST_PARAMETER_ACR_VALUES = "acr_values";

	//OpenID Connect request parameter extensions values
	public final static String OPENID_REQUEST_PARAMETER_SCOPE_OPENID = "openid";
	public final static String OPENID_REQUEST_PARAMETER_SCOPE_PROFILE = "profile";
	public final static String OPENID_REQUEST_PARAMETER_SCOPE_EMAIL = "email";
	public final static String OPENID_REQUEST_PARAMETER_SCOPE_PHONE = "phone";
	public final static String OPENID_REQUEST_PARAMETER_SCOPE_ADDRESS = "address";
	public final static String OPENID_REQUEST_PARAMETER_DISPLAY_PAGE = "page";
	public final static String OPENID_REQUEST_PARAMETER_DISPLAY_POPUP = "popup";
	public final static String OPENID_REQUEST_PARAMETER_DISPLAY_TOUCH = "touch";
	public final static String OPENID_REQUEST_PARAMETER_DISPLAY_WAP = "wap";
	public final static String OPENID_REQUEST_PARAMETER_PROMPT_NONE = "none";
	public final static String OPENID_REQUEST_PARAMETER_PROMPT_LOGIN = "login";
	public final static String OPENID_REQUEST_PARAMETER_PROMPT_CONSENT = "consent";
	public final static String OPENID_REQUEST_PARAMETER_PROMPT_SELECT_ACCOUNT = "select_account";

	//OpenID Connect request parameter extensions values defined in Section 6
	public final static String OPENID_REQUEST_PARAMETER_REQUEST = "request";
	public final static String OPENID_REQUEST_PARAMETER_REQUEST_URI = "request_uri";
	
	//OpenID Connect authorization error response parameters values
	public final static String OPENID_ERROR_RESPONSE_PARAMETER_ERROR_INTERACTION_REQUIRED = "interaction_required";
	public final static String OPENID_ERROR_RESPONSE_PARAMETER_ERROR_LOGIN_REQUIRED = "login_required";
	public final static String OPENID_ERROR_RESPONSE_PARAMETER_ERROR_ACCOUNT_SELECTION_REQUIRED = "account_selection_required";
	public final static String OPENID_ERROR_RESPONSE_PARAMETER_ERROR_CONSENT_REQUIRED = "consent_required";
	public final static String OPENID_ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST_URI = "invalid_request_uri";
	public final static String OPENID_ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST_OBJECT = "invalid_request_object";
	public final static String OPENID_ERROR_RESPONSE_PARAMETER_ERROR_REQUEST_NOT_SUPPORTED = "request_not_supported";
	public final static String OPENID_ERROR_RESPONSE_PARAMETER_ERROR_REQUEST_URI_NOT_SUPPORTED = "request_uri_not_supported";
	public final static String OPENID_ERROR_RESPONSE_PARAMETER_ERROR_REGISTRATION_NOT_SUPPORTED = "registration_not_supported";
}
