package freud.tx;

public enum TransactionType {
    READONLY,
    READWRITE
}
