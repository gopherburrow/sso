package freud.tx;

public class TransactionalResourceException extends RuntimeException {

    private static final long serialVersionUID = 2013020101;

    public TransactionalResourceException() {
        super();
    }

    public TransactionalResourceException(String message, Throwable cause) {
        super(message, cause);
    }

    public TransactionalResourceException(String message) {
        super(message);
    }

    public TransactionalResourceException(Throwable cause) {
        super(cause);
    }
}
