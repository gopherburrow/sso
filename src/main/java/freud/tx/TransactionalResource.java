package freud.tx;

public interface TransactionalResource<Resource> {

    public Resource getResource();

    public void commit() throws TransactionalResourceException;

    public void rollback() throws TransactionalResourceException;

}
