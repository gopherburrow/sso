package freud.tx;

import java.sql.SQLException;

import freud.tx.Transaction;

@FunctionalInterface
public interface TransactionCall<Result> {
	public Result call(Transaction transaction) throws SQLException;
}
