package freud.tx;

public enum TransactionIsolationLevel {
    READ_UNCOMMITED,
    READ_COMMITED,
    REPEATABLE_READ,
    SERIALIZABLE
}
