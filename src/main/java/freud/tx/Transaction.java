package freud.tx;

import static freud.err.Validations.validate;
import static freud.err.Validations.validateNot;
import static freud.err.Validations.validateNotNull;

public class Transaction {

    public static enum ErrorNew {
        TYPE_MUSTBENOTNULL, 
        ISOLATIONLEVEL_MUSTBENOTNULL
    }

    public static enum ErrorAttachTransactionalResource {
        MUSTBENOTFINISHEDTRANSACTION,
        TRANSACTIONALRESOURCE_MUSTBENOTNULL,
        MUSTNOTHAVEAPREVIOUSLYTRANSACTIONALRESOURCEATTACHED
    }

    public static enum ErrorIsResourceOfSameType {
        RESOURCECLASS_MUSTBENOTNULL, 
        MUSTHAVEATRANSACTIONALRESOURCEATTACHED
    }

    public static enum ErrorGetTransactionalResource {
        RESOURCECLASS_MUSTBENOTNULL, 
        MUSTHAVEATRANSACTIONALRESOURCEATTACHED, 
        MUSTHAVESAMETRANSACTIONALRESOURCETYPE
    }

    public static enum ErrorCommit {
        MUSTBENOTFINISHED
    }

    public static enum ErrorRollback {
        MUSTBENOTFINISHED
    }

    private TransactionType type;
    private TransactionIsolationLevel isolationLevel;
    private TransactionalResource<?> transactionalResource;
    private boolean finishedTransaction;

    public Transaction(TransactionType type, TransactionIsolationLevel isolationLevel) {
        validateNotNull(type, ErrorNew.TYPE_MUSTBENOTNULL);
        validateNotNull(isolationLevel, ErrorNew.ISOLATIONLEVEL_MUSTBENOTNULL);

        this.type = type;
        this.isolationLevel = isolationLevel;
        this.transactionalResource = null;
        this.finishedTransaction = false;
    }

    public TransactionType getType() {
        return this.type;
    }

    public TransactionIsolationLevel getIsolationLevel() {
        return this.isolationLevel;
    }

    public void attachTransationalResource(TransactionalResource<?> transactionalResource) {
        validateNotNull(transactionalResource, ErrorAttachTransactionalResource.TRANSACTIONALRESOURCE_MUSTBENOTNULL);
        validateNot(isTransationalResourceAttached(), ErrorAttachTransactionalResource.MUSTNOTHAVEAPREVIOUSLYTRANSACTIONALRESOURCEATTACHED);
        validateNot(this.finishedTransaction, ErrorAttachTransactionalResource.MUSTBENOTFINISHEDTRANSACTION);
        this.transactionalResource = transactionalResource;
    }

    public boolean isTransationalResourceAttached() {
        return this.transactionalResource != null;
    }

    public boolean isResourceOfSameType(Class<?> resourceClass) {
        validateNotNull(resourceClass, ErrorIsResourceOfSameType.RESOURCECLASS_MUSTBENOTNULL);
        validate(isTransationalResourceAttached(), ErrorIsResourceOfSameType.MUSTHAVEATRANSACTIONALRESOURCEATTACHED);
        return resourceClass.isAssignableFrom(this.transactionalResource.getResource().getClass());
    }

    @SuppressWarnings("unchecked")
    public <ResourceClass> ResourceClass getTransactionalResource(Class<ResourceClass> resourceClass) {
        validateNotNull(resourceClass, ErrorGetTransactionalResource.RESOURCECLASS_MUSTBENOTNULL);
        validate(isTransationalResourceAttached(), ErrorGetTransactionalResource.MUSTHAVEATRANSACTIONALRESOURCEATTACHED);
        validate(isResourceOfSameType(resourceClass), ErrorGetTransactionalResource.MUSTHAVESAMETRANSACTIONALRESOURCETYPE);

        return (ResourceClass) this.transactionalResource.getResource();
    }

    public void commit() {
        validateNot(this.finishedTransaction, ErrorCommit.MUSTBENOTFINISHED);
        if (this.transactionalResource == null) {
            return;
        }

        this.transactionalResource.commit();
        this.finishedTransaction = true;
    }

    public void rollback() {
        validateNot(this.finishedTransaction, ErrorRollback.MUSTBENOTFINISHED);
        if (this.transactionalResource == null) {
            return;
        }

        this.transactionalResource.rollback();
        this.finishedTransaction = true;
    }

}
