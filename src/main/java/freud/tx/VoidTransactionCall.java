package freud.tx;

import java.sql.SQLException;

import freud.tx.Transaction;

@FunctionalInterface
public interface VoidTransactionCall {
	public void call(Transaction transaction) throws SQLException;
}
