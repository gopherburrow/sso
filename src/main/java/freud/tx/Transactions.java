package freud.tx;

public class Transactions {
	
	private Transactions() {
		//INFO Cannot be instatiated. 
	}
	
	public static <Result> Result getOnTx(TransactionType transactionType, TransactionIsolationLevel isolationLevel, TransactionCall<Result> call) {
		Transaction transaction = new Transaction(transactionType, isolationLevel);
		try {
			Result result = (Result) call.call(transaction);
			transaction.commit();
			return result;
		} catch(RuntimeException e) {
			transaction.rollback();
			throw e;
		} catch(Exception e) {
			transaction.rollback();
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public static void onTx(TransactionType transactionType, TransactionIsolationLevel isolationLevel, VoidTransactionCall call) {
		Transaction transaction = new Transaction(transactionType, isolationLevel);
		try {
			call.call(transaction);
			transaction.commit();
		} catch(RuntimeException e) {
			transaction.rollback();
			throw e;
		} catch(Exception e) {
			transaction.rollback();
			throw new RuntimeException(e.getMessage(), e);
		}
	}
}
