package freud.tx.sql;

import static freud.err.Exceptions.ignore;
import static freud.err.Validations.validateNotNull;

import java.sql.Connection;
import java.sql.SQLException;

import freud.tx.TransactionalResource;
import freud.tx.TransactionalResourceException;

public class ConnectionTransactionalResource implements TransactionalResource<Connection> {

    public enum ErrorNew {
        CONNECTION_MUSTBENOTNULL
    }

    private Connection connection;

    public ConnectionTransactionalResource(Connection connection) {
        validateNotNull(connection, ErrorNew.CONNECTION_MUSTBENOTNULL);
        this.connection = connection;
    }

    @Override
    public Connection getResource() {
        return this.connection;
    }

    @Override
    public void commit() {
        try {
            this.connection.commit();
        } catch (SQLException e) {
            throw new TransactionalResourceException(e);
        } finally {
        	ignore(()->this.connection.close());
        }
    }

    @Override
    public void rollback() {
        try {
            this.connection.rollback();
        } catch (SQLException e) {
            throw new TransactionalResourceException(e);
        } finally {
        	ignore(()->this.connection.close());
        }
    }
}
