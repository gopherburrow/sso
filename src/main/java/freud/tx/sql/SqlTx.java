package freud.tx.sql;

import static freud.err.Validations.validateNotNull;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import freud.err.EnumException;
import freud.tx.Transaction;
import freud.tx.TransactionIsolationLevel;
import freud.tx.TransactionType;

public class SqlTx {

    public static enum ErrorGetConnection {
        DATASOURCE_MUSTBENOTNULL, 
        TRANSACTION_MUSTBENOTNULL, 
        MUSTHAVESAMETRANSATIONALRESOURCEATTACHED
    }

    private SqlTx() {
        throw new UnsupportedOperationException("freud.tx.sql.SqlTx cannot be instantiated.");
    }

    public static Connection getConnection(DataSource dataSource, Transaction transaction) throws SQLException {
        validateNotNull(dataSource, ErrorGetConnection.DATASOURCE_MUSTBENOTNULL);
        validateNotNull(transaction, ErrorGetConnection.TRANSACTION_MUSTBENOTNULL);

        if (transaction.isTransationalResourceAttached()) {
            if (transaction.isResourceOfSameType(Connection.class)) {
                return transaction.getTransactionalResource(Connection.class);
            }

            throw new EnumException(ErrorGetConnection.MUSTHAVESAMETRANSATIONALRESOURCEATTACHED);
        }

        // New Connection
        Connection connection = dataSource.getConnection();
        connection.setReadOnly(transaction.getType() == TransactionType.READONLY);
        int jdbcTransactionIsolationLevel = getJdbcTransactionIsolationLevel(transaction.getIsolationLevel());
        connection.setTransactionIsolation(jdbcTransactionIsolationLevel);
        connection.setAutoCommit(false);
        ConnectionTransactionalResource connectionTransactionalResource = new ConnectionTransactionalResource(connection);
        transaction.attachTransationalResource(connectionTransactionalResource);
        return connection;
    }

    private static int getJdbcTransactionIsolationLevel(TransactionIsolationLevel transactionIsolationLevel) {
        switch (transactionIsolationLevel) {
        case READ_UNCOMMITED:
            return Connection.TRANSACTION_READ_UNCOMMITTED;
        case REPEATABLE_READ:
            return Connection.TRANSACTION_REPEATABLE_READ;
        case SERIALIZABLE:
            return Connection.TRANSACTION_SERIALIZABLE;
        case READ_COMMITED:
        default:
            return Connection.TRANSACTION_READ_COMMITTED;
        }
    }
}
