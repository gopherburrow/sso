package freud.cache;

import static freud.err.Validations.validateNot;
import static freud.err.Validations.validateNotEmpty;
import static freud.err.Validations.validateNotNull;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.TreeMap;
import java.util.function.Function;

public class Cache<Entity, EntityKey extends Comparable<EntityKey>> {

	private static class CacheItem<Entity> {
		public boolean writing;
		public boolean cached;
		public Instant expiration;
		public Entity entity;
		public CacheItem() {
			super();
			this.writing = false;
			this.writing = false;
			this.expiration = null;
			this.entity = null;
		}
	}
	
	public static enum ErrorNew {
		CACHEEXPIRATION_MUSTBENOTNULL,
		CACHEEXPIRATION_MUSTHAVEPOSITIVEDURATION
	}

	public static enum ErrorGet {
		LOADFN_MUSTBENOTNULL, 
		KEYS_MUSTBENOTEMPTY
	}
	
	private TreeMap<EntityKey[], CacheItem<Entity>> cacheMap;
	private Duration cacheExpiration;

	public Cache(Duration cacheExpiration) {
		validateNotNull(cacheExpiration, ErrorNew.CACHEEXPIRATION_MUSTBENOTNULL);
		validateNot(cacheExpiration.isZero() || cacheExpiration.isNegative(), ErrorNew.CACHEEXPIRATION_MUSTHAVEPOSITIVEDURATION);

		this.cacheMap = new TreeMap<>(Arrays::compare);
		this.cacheExpiration = cacheExpiration;
	}
	
	public Entity get(Function<EntityKey[], Entity> loadFn, @SuppressWarnings("unchecked") EntityKey ... keys) {
		validateNotNull(cacheExpiration, ErrorGet.LOADFN_MUSTBENOTNULL);
		validateNotEmpty(keys, ErrorGet.KEYS_MUSTBENOTEMPTY);
		
		Instant now = Instant.now();
		
		CacheItem<Entity> cacheItem;
		synchronized (this.cacheMap) {
			cacheItem = this.cacheMap.get(keys);
			if(cacheItem == null) {
				cacheItem = new CacheItem<>();
				this.cacheMap.put(keys, cacheItem);
			}
		}

		if((cacheItem.writing && cacheItem.cached) || (cacheItem.expiration != null && now.isBefore(cacheItem.expiration))) {
			return cacheItem.entity;
		}

		synchronized (cacheItem) {
			if(cacheItem.expiration != null && now.isBefore(cacheItem.expiration)) {
				return cacheItem.entity;
			}
			
			cacheItem.writing = true;
			cacheItem.entity = loadFn.apply(keys);
			cacheItem.expiration = Instant.now().plus(this.cacheExpiration);
			cacheItem.cached = true;
			cacheItem.writing = false;
			return cacheItem.entity;
		}	
	}
}
