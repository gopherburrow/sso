package freud.http;

import java.util.Base64;

import spark.Request;
import sso.entity.UsernamePasswordCredentials;

public class Headers {

	private static final String AUTHORIZATION = "Authorization";
	private static final String AUTHORIZATION_BASIC_PREFIX = "Basic ";
	private static final String AUTHORIZATION_BEARER_PREFIX = "Bearer ";
	
	private static final String ERR_NEW_UNSUPPORTED = "microsvc.http.Headers.new.unsupported";

	private Headers() {
		throw new UnsupportedOperationException(ERR_NEW_UNSUPPORTED);
	}

	public static String xForwardedFor(Request req) {
		return req.headers("X-Forwarded-For");
	}

	public static UsernamePasswordCredentials authorizationBasic(Request req) {
		var authorizationHeader = req.headers(AUTHORIZATION);
		if(authorizationHeader == null || !authorizationHeader.startsWith(AUTHORIZATION_BASIC_PREFIX)) {
			return null;
		}
		var basicCredentials = authorizationHeader.substring(AUTHORIZATION_BASIC_PREFIX.length());
		
		String decodedClientCredentials;
		try {
			var clientCredentialsBase64 = Base64.getDecoder().decode(basicCredentials);
			decodedClientCredentials = new String(clientCredentialsBase64, "utf-8");
		} catch (Exception e) {
			return null;
		}
		var splitPos = decodedClientCredentials.indexOf(":");
		if (splitPos <= 0 ) {
			return null;
		}
		return new UsernamePasswordCredentials(decodedClientCredentials.substring(0, splitPos), decodedClientCredentials.substring(splitPos));
	}

	public static String authorizationBearer(Request req) {
		var authorizationHeader = req.headers(AUTHORIZATION);
		if(authorizationHeader == null || !authorizationHeader.startsWith(AUTHORIZATION_BEARER_PREFIX)) {
			return null;
		}
		return authorizationHeader.substring(AUTHORIZATION_BEARER_PREFIX.length());
	}
}
