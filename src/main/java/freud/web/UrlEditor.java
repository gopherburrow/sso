package freud.web;

import static freud.err.Exceptions.re;

import java.lang.reflect.Array;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlEditor {

	public enum NormalizeMode {
		ADD, DELETE
	}

	public static final String REGEX_PROTOCOL = "(?<protocol>[a-zA-Z][a-zA-Z0-9+.-]*)";
	public static final String REGEX_USERPRINCIPAL = "(?<userPrincipal>[a-zA-Z][a-zA-Z0-9+.~-]*)";
	public static final String REGEX_USERPASSWORD = "(?<userPassword>[a-zA-Z0-9+.~-]*)";
	public static final String REGEX_USERINFO = REGEX_USERPRINCIPAL + "(?::" + REGEX_USERPASSWORD + ")?";
	public static final String REGEX_DOMAINNAMEPART = "[a-zA-Z][a-zA-Z0-9_~-]*";
	public static final String REGEX_HOSTNAME = "(?:" + REGEX_DOMAINNAMEPART + "(?:\\." + REGEX_DOMAINNAMEPART + ")*)";
	public static final String REGEX_IPV4ADDRESSDECIMALPART = "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])";
	public static final String REGEX_IPV4ADDRESS = "(?:(?:" + REGEX_IPV4ADDRESSDECIMALPART + "\\.){3}" + REGEX_IPV4ADDRESSDECIMALPART + ")";
	public static final String REGEX_PORT = "(?<port>[0-9]{1,5})";
	public static final String REGEX_HOST = "(?<host>" + REGEX_HOSTNAME + "|" + REGEX_IPV4ADDRESS + ")";
	public static final String REGEX_AUTHORITY = "//(?:" + REGEX_USERINFO + "@)?" + REGEX_HOST + "(?::" + REGEX_PORT + ")?";
	public static final String REGEX_DIRPART = "[a-zA-Z][.#%a-zA-Z0-9_~&=-]*/";
	public static final String REGEX_FILEPART = "[a-zA-Z][.#%a-zA-Z0-9_~&=-]*";
	public static final String REGEX_PATH = "(?<path>/?(?:" + REGEX_DIRPART + ")*(?:" + REGEX_FILEPART + ")?)";
	public static final String REGEX_QUERYPARAMETERNAME = "[a-zA-Z][a-zA-Z0-9_~-]*";
	public static final String REGEX_QUERYPARAMETERVALUE = "[:\\.a-zA-Z0-9/\\?\\|(), %+_~\\=-]*";
	public static final String REGEX_QUERYPARAMETER = REGEX_QUERYPARAMETERNAME + "(?:=" + REGEX_QUERYPARAMETERVALUE + ")?";
	public static final String REGEX_QUERYPARAMETERS = "(?<queryParameters>" + REGEX_QUERYPARAMETER + "(?:&" + REGEX_QUERYPARAMETER + ")*)";
	public static final String REGEX_FRAGMENT = "(?<fragment>[/.#%a-zA-Z0-9_~&=-]*)";
	public static final String REGEX_URL = "(?:" + REGEX_PROTOCOL + ":" + REGEX_AUTHORITY + ")?(?:" + REGEX_PATH + ")?(?:\\?" + REGEX_QUERYPARAMETERS + ")?(?:#" + REGEX_FRAGMENT + ")?";

	private static final Pattern PATTERN_URL = Pattern.compile(REGEX_URL);
	private static final Pattern PATTERN_PROTOCOL = Pattern.compile(REGEX_PROTOCOL);
	private static final Pattern PATTERN_USERPRINCIPAL = Pattern.compile(REGEX_USERPRINCIPAL);
	private static final Pattern PATTERN_USERPASSWORD = Pattern.compile(REGEX_USERPASSWORD);
	private static final Pattern PATTERN_HOST = Pattern.compile(REGEX_HOST);
	private static final Pattern PATTERN_PORT = Pattern.compile(REGEX_PORT);
	private static final Pattern PATTERN_PATH = Pattern.compile(REGEX_PATH);
	private static final Pattern PATTERN_QUERYPARAMETERNAME = Pattern.compile(REGEX_QUERYPARAMETERNAME);
	private static final Pattern PATTERN_QUERYPARAMETERVALUE = Pattern.compile(REGEX_QUERYPARAMETERVALUE);
	private static final Pattern PATTERN_QUERYPARAMETER = Pattern.compile(REGEX_QUERYPARAMETER);
	private static final Pattern PATTERN_QUERYPARAMETERS = Pattern.compile(REGEX_QUERYPARAMETERS);

	private String url;
	private String protocol;
	private String userPrincipal;
	private String userPassword;
	private String host;
	private String port;
	private String path;
	private List<QueryParameter> queryParameters;
	private String fragment;

	public static String normalizeParameters(String url, NormalizeMode normalizeMode, String... parametersNamesAndDefaultValues) {
		if (url == null) {
			// TODO Error message.
			throw new IllegalArgumentException();
		}

		if (normalizeMode == null) {
			// TODO Error message.
			throw new IllegalArgumentException();
		}

		UrlEditor urlEditor = new UrlEditor(url);
		String[] parametersNames = extractParameterNamesFromArray(parametersNamesAndDefaultValues);
		String[] parametersValues = extractParameterValuesFromArray(parametersNamesAndDefaultValues);
		int parametersNamesLength = parametersNames.length;
		int parametersValuesLength = parametersValues.length;
		if (parametersNamesLength != parametersValuesLength) {
			// TODO Error message.
			throw new IllegalArgumentException();
		}
		boolean redirect = false;
		for (int i = 0; i < parametersValues.length; i++) {
			String parameterName = parametersNames[i];
			String parameterValue = parametersValues[i];
			if (normalizeMode == NormalizeMode.ADD) {
				redirect |= urlEditor.addDefaultQueryParameter(parameterName, parameterValue);
				continue;
			}
			redirect |= urlEditor.removeDefaultQueryParameter(parameterName, parameterValue);
		}
		if (!redirect) {
			return null;
		}
		urlEditor.sortQueryParameters();
		return urlEditor.toUrlString();
	}

	public UrlEditor(String url) {
		this.queryParameters = new ArrayList<UrlEditor.QueryParameter>();
		setUrl(url);
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		parseUrl(url);
		this.url = url;
	}

	public String getProtocol() {
		return this.protocol;
	}

	public void setProtocol(String protocol) {
		if (protocol != null) {
			createMatcherAndValidateToken(PATTERN_PROTOCOL, protocol, "freud.util.web.url.UrlEditor.protocol.validity");
		}
		this.protocol = protocol;
	}

	public String getUserPrincipal() {
		return this.userPrincipal;
	}

	public void setUserPrincipal(String userPrincipal) {
		if (userPrincipal != null) {
			createMatcherAndValidateToken(PATTERN_USERPRINCIPAL, userPrincipal,
					"freud.util.web.url.UrlEditor.userPrincipal.validity");
		}
		this.userPrincipal = userPrincipal;
	}

	public String getUserPassword() {
		return this.userPassword;
	}

	public void setUserPassword(String userPassword) {
		if (userPassword != null) {
			createMatcherAndValidateToken(PATTERN_USERPASSWORD, userPassword,
					"freud.util.web.url.UrlEditor.userPassword.validity");
		}
		this.userPassword = userPassword;
	}

	public String getHost() {
		return this.host;
	}

	public void setHost(String host) {
		if (host != null) {
			createMatcherAndValidateToken(PATTERN_HOST, host, "freud.util.web.url.UrlEditor.host.validity");
		}
		this.host = host;
	}

	public boolean isAbsolute() {
		return this.protocol != null && !this.protocol.isEmpty() && this.host != null && !this.host.isEmpty();
	}

	public String getPort() {
		return this.port;
	}

	public void setPort(String port) {
		if (port != null) {
			createMatcherAndValidateToken(PATTERN_PORT, port, "freud.util.web.url.UrlEditor.port.validity");
		}
		this.port = port;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		if (path != null) {
			createMatcherAndValidateToken(PATTERN_PATH, path, "freud.util.web.url.UrlEditor.path.validity");
		}
		this.path = path;
	}

	public boolean isPathAbsolute() {
		 return this.path.startsWith("/") || isAbsolute() && path.isEmpty();
	}

	public String getQueryParameters() {
		StringBuilder queryParametersStringBuilder = new StringBuilder();
		int queryParametersSize = this.queryParameters.size();
		for (int queryParameterIndex = 0; queryParameterIndex < queryParametersSize; queryParameterIndex++) {
			QueryParameter queryParameter = this.queryParameters.get(queryParameterIndex);
			boolean addSeparator = queryParameterIndex > 0;
			if (addSeparator) {
				queryParametersStringBuilder.append("&");
			}
			queryParametersStringBuilder.append(re(()->URLEncoder.encode(queryParameter.getName(), "utf-8")) + "=" + re(()->URLEncoder.encode(queryParameter.getValue(), "utf-8")));
		}
		return queryParametersStringBuilder.toString();
	}

	public void setQueryParameters(String queryParameters) {
		if (queryParameters == null) {
			this.queryParameters.clear();
			return;
		}

		createMatcherAndValidateToken(PATTERN_QUERYPARAMETERS, queryParameters,
				"freud.util.web.url.UrlEditor.queryParameters.validity");
		String[] queryParameterTuples = queryParameters.split("&");
		for (String queryParameterTuple : queryParameterTuples) {
			this.addQueryParameter(queryParameterTuple);
		}
	}

	public int getQueryParametersSize() {
		return this.queryParameters.size();
	}

	public String getQueryParameter(int queryParameterIndex) {
		return this.queryParameters.get(queryParameterIndex).toString();
	}

	public void addQueryParameter(String queryParameterTuple) {
		createMatcherAndValidateToken(PATTERN_QUERYPARAMETER, queryParameterTuple,
				"freud.util.web.url.UrlEditor.queryParameter.tuple.validity");
		int firstSeparatorIndex = queryParameterTuple.indexOf("=");
		boolean hasValue = firstSeparatorIndex != -1;
		String queryParameterName = URLDecoder.decode(hasValue ? queryParameterTuple.substring(0, firstSeparatorIndex)
				: queryParameterTuple, Charset.forName("US-ASCII"));
		String queryParameterValue = URLDecoder.decode(hasValue ? queryParameterTuple.substring(firstSeparatorIndex + 1) : "", Charset.forName("US-ASCII"));;
		QueryParameter queryParameter = new QueryParameter(queryParameterName, queryParameterValue);
		this.queryParameters.add(queryParameter);
	}

	public void removeQueryParameter(String... queryParameterNames) {
		List<String> queryParameterNameList = Arrays.asList(queryParameterNames);
		for (Iterator<QueryParameter> queryParametersIterator = this.queryParameters.iterator(); queryParametersIterator
				.hasNext();) {
			QueryParameter queryParameter = queryParametersIterator.next();
			if (queryParameterNameList.contains(queryParameter.getName())) {
				queryParametersIterator.remove();
			}
		}
	}

	public void removeQueryParameterValue(String queryParameterName, String... queryParameterValues) {
		List<String> queryParameterValueList = Arrays.asList(queryParameterValues);
		for (Iterator<QueryParameter> queryParametersIterator = this.queryParameters.iterator(); queryParametersIterator
				.hasNext();) {
			QueryParameter queryParameter = queryParametersIterator.next();
			if (queryParameter.getName().equals(queryParameterName)
					&& queryParameterValueList.contains(queryParameter.getValue())) {
				queryParametersIterator.remove();
			}
		}
	}

	public boolean hasQueryParameter(String queryParameterName) {
		return getQueryParametersByName(queryParameterName).length > 0;
	}

	public String getQueryParameterName(int queryParameterIndex) {
		return this.queryParameters.get(queryParameterIndex).getName();
	}

	public boolean isQueryParameterHasMultipleValues(String queryParameterName) {
		return getQueryParameterValues(queryParameterName).length > 1;
	}

	public String getQueryParameterValue(int queryParameterIndex) {
		return this.queryParameters.get(queryParameterIndex).getValue();
	}

	public String getQueryParameterValue(String queryParameterName) {
		QueryParameter[] queryParameters = getQueryParametersByName(queryParameterName);
		validateQueryParameterExistenceAndUniqueness(queryParameters);
		return queryParameters[0].getValue();
	}

	public void sortQueryParameters() {
		Collections.sort(this.queryParameters, new QueryParameterComparator());
	}

	private void validateQueryParameterExistenceAndUniqueness(QueryParameter[] queryParameters) {
		boolean isQueryParameterExist = queryParameters.length > 0;
		boolean isQueryParameterHasMultipleValues = queryParameters.length > 1;
		if (!isQueryParameterExist) {
			throw new IllegalStateException(
					"freud.util.web.url.UrlEditor.getQueryParameterValue.queryParameterName.existence");
		}
		if (isQueryParameterHasMultipleValues) {
			throw new IllegalStateException("freud.util.web.url.UrlEditor.getQueryParameterValue.result.uniqueness");
		}
	}

	private QueryParameter[] getQueryParametersByName(String queryParameterName) {
		List<QueryParameter> queryParameterList = new ArrayList<>();
		for (QueryParameter queryParameter : this.queryParameters) {
			if (queryParameter.getName().equals(queryParameterName)) {
				queryParameterList.add(queryParameter);
			}
		}
		return queryParameterList.toArray(new QueryParameter[] {});
	}

	public String[] getQueryParameterValues(String queryParameterName) {
		List<String> queryParameterValuesList = new ArrayList<String>();
		QueryParameter[] queryParameters = getQueryParametersByName(queryParameterName);
		for (QueryParameter queryParameter : queryParameters) {
			String value = queryParameter.getValue();
			queryParameterValuesList.add(value);
		}
		return queryParameterValuesList.toArray(new String[] {});
	}

	public void setQueryParameterValues(String queryParameterName, String... queryParameterValues) {
		removeQueryParameter(queryParameterName);
		createMatcherAndValidateToken(PATTERN_QUERYPARAMETERNAME, queryParameterName,
				"freud.util.web.url.UrlEditor.queryParameters.name.validity");
		createMatcherAndValidateToken(PATTERN_QUERYPARAMETERVALUE, queryParameterName,
				"freud.util.web.url.UrlEditor.queryParameters.value.validity");
		for (String queryParameterValue : queryParameterValues) {
			QueryParameter queryParameter = new QueryParameter(queryParameterName, queryParameterValue);
			this.queryParameters.add(queryParameter);
		}
	}

	public boolean addDefaultQueryParameter(String queryParameterName, String queryParameterValue) {
		if (hasQueryParameter(queryParameterName) && !isQueryParameterHasMultipleValues(queryParameterName)
				&& !getQueryParameterValue(queryParameterName).isEmpty()) {
			return false;
		}
		setQueryParameterValues(queryParameterName, queryParameterValue);
		return true;
	}

	public boolean removeDefaultQueryParameter(String queryParameterName, String queryParameterValue) {
		if (!hasQueryParameter(queryParameterName) || isQueryParameterHasMultipleValues(queryParameterName)
				|| !getQueryParameterValue(queryParameterName).equals(queryParameterValue)) {
			return false;
		}
		removeQueryParameter(queryParameterName);
		return true;
	}
	
	public String getFragment() {
		return this.fragment;
	}

	private static Matcher createMatcherAndValidateToken(Pattern pattern, String token, String doesNotMatchMessage) {
		Matcher matcher = pattern.matcher(token);

		if (token == null) {
			return matcher;
		}

		if (!matcher.matches()) {
			// TODO Error message.
			throw new IllegalArgumentException(doesNotMatchMessage);
		}
		return matcher;
	}

	public String toUrlString() {
		StringBuilder urlStringBuilder = new StringBuilder();
		if (this.protocol != null) {
			urlStringBuilder.append(this.protocol);
			urlStringBuilder.append("://");
		}

		if (this.host != null) {
			if (this.userPrincipal != null) {
				urlStringBuilder.append(this.userPrincipal);
				if (this.userPassword != null) {
					urlStringBuilder.append(":");
					urlStringBuilder.append(this.userPassword);
				}
				urlStringBuilder.append("@");
			}

			urlStringBuilder.append(this.host);
			if (this.port != null) {
				urlStringBuilder.append(":");
				urlStringBuilder.append(port);
			}
		}

		if (this.path != null) {
			urlStringBuilder.append(this.path);
		}

		if (!this.queryParameters.isEmpty()) {
			urlStringBuilder.append("?");
			urlStringBuilder.append(this.getQueryParameters());
		}

		if (this.fragment != null) {
			urlStringBuilder.append("#");
			urlStringBuilder.append(this.fragment);
		}

		if (urlStringBuilder.length() == 0) {
			return "?";
		}

		return urlStringBuilder.toString();
	}

	@Override
	public String toString() {
		return toUrlString();
	}

	private void parseUrl(String url) {
		Matcher urlMatcher = createMatcherAndValidateToken(PATTERN_URL, url, "freud.util.web.url.UrlEditor.url.validity (url=" + url + ")");
		setProtocol(urlMatcher.group("protocol"));
		setUserPrincipal(urlMatcher.group("userPrincipal"));
		setUserPassword(urlMatcher.group("userPassword"));
		setHost(urlMatcher.group("host"));
		setPort(urlMatcher.group("port"));
		setPath(urlMatcher.group("path"));
		setQueryParameters(urlMatcher.group("queryParameters"));
		this.fragment = urlMatcher.group("fragment");
	}

	private static <T> T[] extractValueFromArray(int recordSize, int recordOffset, Class<T> type, Object... array) {

		if (recordSize < 1) {
			// TODO Error message.
			throw new IllegalArgumentException();
		}

		if (recordOffset < 0) {
			// TODO Error message.
			throw new IllegalArgumentException();
		}

		if (recordOffset >= recordSize) {
			// TODO Error message.
			throw new IllegalArgumentException();
		}

		if ((array == null) || (array.length == 0)) {
			// TODO Error message.
			throw new IllegalArgumentException();
		}

		if ((array.length % recordSize) != 0) {
			// TODO Error message.
			throw new IllegalArgumentException();
		}

		int valuesSize = array.length / recordSize;

		@SuppressWarnings("unchecked")
		T[] values = (T[]) Array.newInstance(type, valuesSize);

		for (int entryIndex = 0; entryIndex < valuesSize; entryIndex++) {
			int entryInArrayIndex = (entryIndex * recordSize) + recordOffset;
			Object valueObject = array[entryInArrayIndex];

			if ((valueObject != null) && !type.isAssignableFrom(valueObject.getClass())) {
				// TODO Error message.
				throw new IllegalArgumentException();
			}

			@SuppressWarnings("unchecked")
			T value = (T) valueObject;
			values[entryIndex] = value;
		}

		return values;
	}

	private static String[] extractParameterNamesFromArray(String... array) {
		return extractValueFromArray(2, 0, String.class, (Object[]) array);
	}

	private static String[] extractParameterValuesFromArray(String... array) {
		return extractValueFromArray(2, 1, String.class, (Object[]) array);
	}

	private static class QueryParameter {
		public static final String EXCEPTION_NAME_OBLIGATORINESS = "freud.util.web.url.UrlEditor.QueryParameter.name.obligatoriness";
		public static final String EXCEPTION_VALUE_OBLIGATORINESS = "freud.util.web.url.UrlEditor.QueryParameter.value.obligatoriness";

		private String name;
		private String value;

		private static boolean isNameObligatorinessValid(String name) {
			return name != null && !name.isEmpty();
		}

		private static void validateName(String name) {
			if (!QueryParameter.isNameObligatorinessValid(name)) {
				throw new IllegalArgumentException(EXCEPTION_NAME_OBLIGATORINESS);
			}
		}

		private static boolean isValueObligatorinessValid(String value) {
			return value != null;
		}

		private static void validateValue(String value) {
			if (!QueryParameter.isValueObligatorinessValid(value)) {
				throw new IllegalArgumentException(EXCEPTION_VALUE_OBLIGATORINESS);
			}
		}

		public QueryParameter(String name, String value) {
			super();
			setName(name);
			setValue(value);
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			validateName(name);
			this.name = name;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			validateValue(value);
			this.value = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}

			if (obj == null) {
				return false;
			}

			if (getClass() != obj.getClass()) {
				return false;
			}

			QueryParameter other = (QueryParameter) obj;

			if (name == null) {
				if (other.name != null) {
					return false;
				}
			} else if (!name.equals(other.name)) {
				return false;
			}

			if (value == null) {
				if (other.value != null) {
					return false;
				}
			} else if (!value.equals(other.value)) {
				return false;
			}

			return true;
		}

		@Override
		public String toString() {
			StringBuilder queryParameterStringBuilder = new StringBuilder();
			queryParameterStringBuilder.append(URLEncoder.encode(this.name, Charset.forName("US-ASCII")));
			if (this.value != null && !this.value.isEmpty()) {
				queryParameterStringBuilder.append("=");
				queryParameterStringBuilder.append(URLEncoder.encode(this.value, Charset.forName("US-ASCII")));;
			}
			return queryParameterStringBuilder.toString();
		}
	}

	private class QueryParameterComparator implements Comparator<QueryParameter> {
		@Override
		public int compare(QueryParameter o1, QueryParameter o2) {
			int comparationResult = o1.getName().compareTo(o2.getName());
			if (comparationResult != 0) {
				return comparationResult;
			}
			return o1.getValue().compareTo(o2.getValue());
		}
	}

	// public static void main(String[] args) {
	// 	var u = new UrlEditor("/teste");
	// 	System.out.println(u.getProtocol());
	// 	System.out.println(u.getHost());
	// }
}
