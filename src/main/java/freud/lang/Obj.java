package freud.lang;

import java.util.function.Function;

public class Obj {

	private static final String ERR_NEW_UNSUPPORTED = "freud.lang.Obj.new.unsupported";

	private Obj() {
		throw new UnsupportedOperationException(ERR_NEW_UNSUPPORTED);
	}

    public static <T> boolean equals(T obj1, Object obj2, Function<T, Object[]> attributesExtractor) {
        if (obj1 == null) {
            throw new IllegalArgumentException("freud.lang.Obj.equals.obj1.mustBeNonNull");
        }
        if (obj1 == obj2) {
            return true;
        }
        if ((obj2 == null) || (obj1.getClass() != obj2.getClass())) {
            return false;
        }
        @SuppressWarnings("unchecked")
        T castObject2 = (T) obj2;

        Object[] attributes1 = attributesExtractor.apply(obj1);
        Object[] attributes2 = attributesExtractor.apply(castObject2);
        for (int index = 0; index < attributes1.length; index++) {
            Object attribute1 = attributes1[index];
            Object attribute2 = attributes2[index];

            if ((attribute1 == null) ^ (attribute2 == null)) {
                return false;
            }

            if (attribute1 == null) {
                continue;
            }

            if (!attribute1.equals(attribute2)) {
                return false;
            }
        }
        return true;
    }

    public static <T> int hashCode(T obj, Function<T, Object[]> attributesExtractor) {
        final int prime = 31;
        int result = 1;
        for (Object attribute : attributesExtractor.apply(obj)) {
            result = (result * prime) + ((attribute == null) ? 0 : attribute.hashCode());
        }
        return result;
    }

    public static <T> String toString(T obj, Function<T, Object[]> attributesExtractor, String ... attributeNames) {
    	Object[] values = attributesExtractor.apply(obj);
    	if(attributeNames.length != values.length) {
    		throw new IllegalArgumentException("freud.lang.Obj.toString.attributeNames.mustHaveSameLengthAsAttributesExtractorValues");
    	}
    	String[] tupleArray = new String[values.length];
        for (int i = 0; i < attributeNames.length; i++) {
			String name = attributeNames[i];
			Object value = values[i];
			tupleArray[i] = name + "=" + value.toString();
		}
        return "{" + String.join(", ", tupleArray) +"}";
    }
    
    public static <T> int compareTo(T obj1, T obj2, Function<T, Comparable<?>[]> attributesExtractor) {
        Comparable<?>[] obj1Attributes = attributesExtractor.apply(obj1);
        Comparable<?>[] obj2Attributes = attributesExtractor.apply(obj2);
        int attributesLength = obj1Attributes.length;
        for (int attributeIndex = 0; attributeIndex < attributesLength; attributeIndex++) {
            @SuppressWarnings("rawtypes")
            Comparable obj1Attribute = obj1Attributes[attributeIndex];
            @SuppressWarnings("rawtypes")
            Comparable obj2Attribute = obj2Attributes[attributeIndex];
            @SuppressWarnings("unchecked")
            int result = obj1Attribute.compareTo(obj2Attribute);
            if (result != 0) {
                return result;
            }
        }
        return 0;
    }

    public static Object[] attributes(Object... attributes) {
        return attributes;
    }

    public static Comparable<?>[] comparableAttributes(Comparable<?>... attributes) {
        return attributes;
    }
}
