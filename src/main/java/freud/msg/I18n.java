package freud.msg;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class I18n {
	private static final String BUNDLE_NAME = "messages"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	private I18n() {
		throw new UnsupportedOperationException("freud.msg.I18n cannot be instatiated");
	}

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return key;
		}
	}
	
	public static String getString(String key, Object ... parameters) {
		try {
			MessageFormat messageFormat = new MessageFormat(RESOURCE_BUNDLE.getString(key));
			return messageFormat.format(parameters);
		} catch (MissingResourceException e) {
			return key;
		}
	}

	public static String getString(String key, Locale locale, Object ... parameters) {
		try {
			MessageFormat messageFormat = new MessageFormat(RESOURCE_BUNDLE.getString(key));
			messageFormat.setLocale(locale);
			return messageFormat.format(parameters);
		} catch (MissingResourceException e) {
			return key;
		}
	}

}