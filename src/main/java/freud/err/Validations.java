package freud.err;

import java.util.Collection;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import freud.web.UrlEditor;

public class Validations {

	private static final String ERR_NEW_UNSUPPORTED = "freud.err.Validations.new.unsupported";

	// Borrowed from RFC 822
	public static final String EMAIL_ADDRESS_REGEX = "(?:(?:\\r\\n)?[ \\t])*(?:(?:(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*\\<(?:(?:\\r\\n)?[ \\t])*(?:@(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*(?:,@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*)*:(?:(?:\\r\\n)?[ \\t])*)?(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*\\>(?:(?:\\r\\n)?[ \\t])*)|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*:(?:(?:\\r\\n)?[ \\t])*(?:(?:(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*\\<(?:(?:\\r\\n)?[ \\t])*(?:@(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*(?:,@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*)*:(?:(?:\\r\\n)?[ \\t])*)?(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*\\>(?:(?:\\r\\n)?[ \\t])*)(?:,\\s*(?:(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*\\<(?:(?:\\r\\n)?[ \\t])*(?:@(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*(?:,@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*)*:(?:(?:\\r\\n)?[ \\t])*)?(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*\\>(?:(?:\\r\\n)?[ \\t])*))*)?;\\s*)";
	
	private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_ADDRESS_REGEX);
	private static final Pattern URL_PATTERN = Pattern.compile(UrlEditor.REGEX_URL);

	private Validations() {
		throw new UnsupportedOperationException(ERR_NEW_UNSUPPORTED);
	}

	public static <E extends Enum<E>> void validate(boolean value, E enumValue, String ... extraInfo) {
		if (!value) {
			throw new EnumException(enumValue, extraInfo);
		}
	}

	public static <E extends Enum<E>> void validateNot(boolean value, E enumValue, String ... extraInfo) {
		if (value) {
			throw new EnumException(enumValue, extraInfo);
		}
	}

	public static <E extends Enum<E>> void validate(Supplier<Boolean> falseGeneratesErrorFunction, E enumValue, String ... extraInfo) {
		validate(falseGeneratesErrorFunction.get().booleanValue(), enumValue, extraInfo);
	}

	public static <E extends Enum<E>> void validateNot(Supplier<Boolean> trueGeneratesErrorFunction, E enumValue, String ... extraInfo) {
		validateNot(trueGeneratesErrorFunction.get().booleanValue(), enumValue, extraInfo);
	}

//	public static <Value, E extends Enum<E>> void validateOptional(Value value, Supplier<Boolean> falseGeneratesErrorFunction, E enumValue, String ... extraInfo) {
//		if (value == null) {
//			return;
//		}
//		validate(falseGeneratesErrorFunction, enumValue);
//	}
//
//	public static <Value, E extends Enum<E>> void validateOptional(Value value, Runnable otherValidateMethod) {
//		if (value == null) {
//			return;
//		}
//		otherValidateMethod.run();
//	}

	public static <Value, E extends Enum<E>> void validateNull(Value value, E enumValue, String ... extraInfo) {
		validate(()->value == null, enumValue, extraInfo);
	}

	public static <Value, E extends Enum<E>> void validateNotNull(Value value, E enumValue, String ... extraInfo) {
		validate(()->value != null, enumValue, extraInfo);
	}

	public static <E extends Enum<E>> void validateNotEmpty(String value, E enumValue, String ... extraInfo) {
		validate(()-> value != null && !value.isEmpty(), enumValue, extraInfo);
	}

	public static <Value, E extends Enum<E>> void validateNotEmpty(Collection<Value> value, E enumValue, String ... extraInfo) {
		validate(()-> value != null && !value.isEmpty(), enumValue, extraInfo);
	}

	public static <Value, E extends Enum<E>> void validateNotEmpty(Value[] value, E enumValue, String ... extraInfo) {
		validate(()-> value != null && value.length > 0, enumValue, extraInfo);
	}

	public static <E extends Enum<E>> void validateSize(int size, String value, E enumValue, String ... extraInfo) {
		validate(()-> value.length() == size, enumValue, extraInfo);
	}

	public static <Value, E extends Enum<E>> void validateSize(int size, Collection<Value> value, E enumValue, String ... extraInfo) {
		validate(()-> value.size() == size, enumValue, extraInfo);
	}

	public static <Value, E extends Enum<E>> void validateSize(int size, Value[] value, E enumValue, String ... extraInfo) {
		validate(()-> value.length == size, enumValue, extraInfo);
	}

	public static <E extends Enum<E>> void validateMinLength(int minLength, String value, E enumValue, String ... extraInfo) {
		validateNot(()-> value.length() < minLength, enumValue, extraInfo);
	}

	public static <Value, E extends Enum<E>> void validateMinLength(int minLength, Collection<Value> value, E enumValue, String ... extraInfo) {
		validateNot(()-> value.size() < minLength, enumValue, extraInfo);
	}

	public static <Value, E extends Enum<E>> void validateMinLength(int minLength, Value[] value, E enumValue, String ... extraInfo) {
		validateNot(()-> value.length < minLength, enumValue, extraInfo);
	}

	public static <E extends Enum<E>> void validateMaxLength(int maxLength, String value, E enumValue, String ... extraInfo) {
		validateNot(()-> value.length() > maxLength, enumValue, extraInfo);
	}

	public static <Value, E extends Enum<E>> void validateMaxLength(int maxLength, Collection<Value> value, E enumValue, String ... extraInfo) {
		validateNot(()-> value.size() > maxLength, enumValue, extraInfo);
	}

	public static <Value, E extends Enum<E>> void validateMaxLength(int maxLength, Value[] value, E enumValue, String ... extraInfo) {
		validateNot(()-> value.length > maxLength, enumValue, extraInfo);
	}

	public static <E extends Enum<E>> void validateRegex(String contentPattern, String value, E enumValue, String ... extraInfo) {
		validate(Pattern.matches(contentPattern, value), enumValue, extraInfo);
	}

	public static <E extends Enum<E>> void validateRegex(Pattern contentPattern, String value, E enumValue, String ... extraInfo) {
		validate(contentPattern.matcher(value).matches(), enumValue, extraInfo);
	}

	public static <E extends Enum<E>> void validateEmailAddress(String value, E enumValue, String ... extraInfo) {
		validate(EMAIL_PATTERN.matcher(value).matches(), enumValue, extraInfo);
	}

	public static <E extends Enum<E>> void validateUrl(String value, E enumValue, String ... extraInfo) {
		validate(URL_PATTERN.matcher(value).matches(), enumValue, extraInfo);
	}
	
	public static <Type, E extends Enum<E>> Type validateType(Object value, Class<Type> type, E enumValue, String ... extraInfo) {
		validate(value.getClass() == type, enumValue, extraInfo);
		@SuppressWarnings("unchecked")
		Type result = (Type) value;
		return result;
	}

//	public static void validateOptionalSize(int size, String value, E enumValue) {
//		validateOptional(value, ()->validateSize(size, value, enumValue));
//	}
//
//	public static <Value> void validateOptionalSize(int size, Collection<Value> value, E enumValue) {
//		validateOptional(value, ()->validateSize(size, value, enumValue));
//	}
//
//	public static <Value> void validateOptionalSize(int size, Value[] value, E enumValue) {
//		validateOptional(value, ()->validateSize(size, value, enumValue));
//	}
//
//	public static void validateOptionalMinLength(int minLength, String value, E enumValue) {
//		validateOptional(value, ()->validateMinLength(minLength, value, enumValue));
//	}
//
//	public static <Value> void validateOptionalMinLength(int minLength, Collection<Value> value, E enumValue) {
//		validateOptional(value, ()->validateMinLength(minLength, value, enumValue));
//	}
//
//	public static <Value> void validateOptionalMinLength(int minLength, Value[] value, E enumValue) {
//		validateOptional(value, ()->validateMinLength(minLength, value, enumValue));
//	}
//	
//	public static void validateOptionalMaxLength(int maxLength, String value, E enumValue) {
//		validateOptional(value, ()->validateMaxLength(maxLength, value, enumValue));
//	}
//
//	public static <Value> void validateOptionalMaxLength(int maxLength, Collection<Value> value, E enumValue) {
//		validateOptional(value, ()->validateMaxLength(maxLength, value, enumValue));
//	}
//
//	public static <Value> void validateOptionalMaxLength(int maxLength, Value[] value, E enumValue) {
//		validateOptional(value, ()->validateMaxLength(maxLength, value, enumValue));
//	}
//
//	public static void validateOptionalRegex(String contentPattern, String value, E enumValue) {
//		validateOptional(value, ()->validateRegex(contentPattern, value, enumValue));
//	}
//	
//	public static void validateOptionalRegex(Pattern contentPattern, String value, E enumValue) {
//		validateOptional(value, ()->validateRegex(contentPattern, value, enumValue));
//	}
//	
//	public static void validateOptionalEmailAddress(String value, E enumValue) {
//		validateOptional(value, ()->validateEmailAddress(value, enumValue));
//	}
}