package freud.err;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.function.Consumer;
import java.util.function.Function;

public class Exceptions {
	
	private static final String ERR_NEW_UNSUPPORTED = "freud.err.Exceptions.new.unsupported";

	private Exceptions() {
		throw new UnsupportedOperationException(ERR_NEW_UNSUPPORTED);
	}
	
	private static Consumer<Exception> logIgnore;
	
	public static void setLogIgnore(Consumer<Exception> logIgnore) {
		Exceptions.logIgnore = logIgnore; 
	}
	
	public static interface ExceptionRunnable {
		void run() throws Exception;
	}

	public static interface ExceptionSupplier<T> {
		T get() throws Exception;
	}

	public static void re(ExceptionRunnable runnable) {
		try {
			runnable.run();
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public static <T> T re(ExceptionSupplier<T> supplier) {
		try {
			return supplier.get();
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	public static void ignore(ExceptionRunnable runnable) {
		try {
			runnable.run();
		} catch (Exception e) {
			if(Exceptions.logIgnore == null) {
				return;
			}
			Exceptions.logIgnore.accept(e);
		}
	}
	
	public static void translate(ExceptionRunnable runnable, Function<Exception, RuntimeException> translatedException) {
		try {
			runnable.run();
		} catch(Exception e) {
			throw translatedException.apply(e);
		}
	}

	public static <T> T translate(ExceptionSupplier<T> supplier, Function<Exception, RuntimeException> translatedException) {
		try {
			return supplier.get();
		} catch(Exception e) {
			throw translatedException.apply(e);
		}
	}

	public static void translate(ExceptionRunnable runnable, ExceptionTranslation ...translations) {
		try {
			runnable.run();
		} catch(RuntimeException e) {
			for (ExceptionTranslation translation : translations) {
				if (translation.exceptionPredicate.test(e)) {
					throw translation.translateExceptionFn.apply(e);
				}
			}
			throw e;
		} catch(Exception e) {
			for (ExceptionTranslation translation : translations) {
				if (translation.exceptionPredicate.test(e)) {
					throw translation.translateExceptionFn.apply(e);
				}
			}
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public static <R, ToException extends RuntimeException> R translate(ExceptionSupplier<R> supplier, ExceptionTranslation ...translations) {
		try {
			return supplier.get();
		} catch(RuntimeException e) {
			for (ExceptionTranslation translation : translations) {
				if (translation.exceptionPredicate.test(e)) {
					throw translation.translateExceptionFn.apply(e);
				}
			}
			throw e;
		} catch(Exception e) {
			for (ExceptionTranslation translation : translations) {
				if (translation.exceptionPredicate.test(e)) {
					throw translation.translateExceptionFn.apply(e);
				}
			}
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public static String stackTraceToString(Exception e) {
		var out = new StringWriter();
		var pw = new PrintWriter(out);
		e.printStackTrace(pw);
		pw.close();
		ignore(()->out.close());
		return out.toString();
	}
}
