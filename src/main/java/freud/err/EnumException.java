package freud.err;

public class EnumException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public Enum<?> value;
	public String[] extraInfo;
	
	public EnumException(Enum<?> value, Throwable cause, String ... extraInfo) {
		super(value.getDeclaringClass().getName() + "." + value.name(), cause, true, false);
		this.value = value; 
	}

	public EnumException(Enum<?> value, String ... extraInfo) {
		this(value, (Throwable) null);
		this.extraInfo = extraInfo;
	}
}
