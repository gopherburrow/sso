package freud.err;

import java.util.function.Function;
import java.util.function.Predicate;

public class ExceptionTranslation {
	
	public static ExceptionTranslation byMsg(String msg, Function<Exception, RuntimeException> translateExceptionFn) {
		return new ExceptionTranslation(e->msg.equals(e.getMessage()), translateExceptionFn);
	}

	public static <T extends Exception> ExceptionTranslation byType(Class<T> type, Function<Exception, RuntimeException> translateExceptionFn) {
		return new ExceptionTranslation(e->type.isAssignableFrom(e.getClass()), translateExceptionFn);
	}
	
	public static ExceptionTranslation by(Predicate<Exception> exceptionPredicate, Function<Exception, RuntimeException> translateExceptionFn) {
		return new ExceptionTranslation(exceptionPredicate, translateExceptionFn);
	}

	public static ExceptionTranslation any(Function<Exception, RuntimeException> translatedException) {
		return new ExceptionTranslation(e->true, translatedException);
	}

	public static ExceptionTranslation byEnum(Enum<?> from, Enum<?> to, String ... extraInfo) {
		return new ExceptionTranslation(e->e instanceof EnumException && ((EnumException) e).value == from, e->new EnumException(to, e, extraInfo));
	}

	public static ExceptionTranslation byEnum(Enum<?> from, Function<EnumException, RuntimeException> translateEnumExceptionFn) {
		Function<Exception, RuntimeException> translateExceptionFn = e->translateEnumExceptionFn.apply((EnumException) e);
		return new ExceptionTranslation(e->e instanceof EnumException && ((EnumException) e).value == from, translateExceptionFn);
	}

	public Predicate<Exception> exceptionPredicate;
	public Function<Exception, RuntimeException> translateExceptionFn;
	
	private ExceptionTranslation(Predicate<Exception> exceptionPredicate, Function<Exception, RuntimeException> translateExceptionFn) {
		super();
		this.exceptionPredicate = exceptionPredicate;
		this.translateExceptionFn = translateExceptionFn;
	}
}

