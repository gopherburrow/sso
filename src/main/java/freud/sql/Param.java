package freud.sql;

import freud.lang.Obj;

public class Param {

    public static String toString(Param[] sqlParameters) {
        int sqlParametersLength = sqlParameters.length;
        if ((sqlParameters == null) || (sqlParametersLength == 0)) {
            return "";
        }
        StringBuilder sb = new StringBuilder("[");
        sb.append(sqlParameters[0].toString());
        for (int sqlParameterIndex = 1; sqlParameterIndex < sqlParametersLength; sqlParameterIndex++) {
            Param param = sqlParameters[sqlParameterIndex];
            sb.append(", ");
            sb.append(param);
        }
        sb.append("]");
        return sb.toString();
    }

    public int parameterIndex;

    public int sqlType;

    public Object value;

    public Param(int parameterIndex, int sqlType, Object value) {
        super();
        this.parameterIndex = parameterIndex;
        this.sqlType = sqlType;
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return Obj.equals(this, obj, o -> Obj.attributes(o.parameterIndex, o.sqlType, o.value));
    }

}
