package freud.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

@FunctionalInterface
public interface ResultSetEntityExtractor<Type> {
    public Type getEntity(ResultSet resultSet) throws SQLException;
}
