package freud.sql.merger;

@FunctionalInterface
public interface Joiner<Instance, EntityKeys extends Comparable<EntityKeys>, AssociatedType> {
    public void join(Instance instance, EntityKeys[] associationKeys, AssociatedType value);
}
