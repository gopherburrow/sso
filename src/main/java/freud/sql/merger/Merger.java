package freud.sql.merger;

import static freud.err.Validations.validate;
import static freud.err.Validations.validateNotEmpty;
import static freud.err.Validations.validateNotNull;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import freud.err.EnumException;

public class Merger {

    public static enum ErrorJoin {
        MAINENTITYLIST_MUSTBENOTNULL, 
        MAINENTITYKEYEXTRACTOR_MUSTBENOTNULL, 
        SUBENTITYLIST_MUSTBENOTNULL,
        SUBENTITYKEYEXTRACTOR_MUSTBENOTNULL, 
        JOINER_MUSTBENOTNULL,
        MAINENTITYLIST_MUSTBENOTEMPTYWHENSUBENTITYLISTHASELEMENTS, 
        SUBENTITYLIST_MUSTHAVEALLMAINENTITYELEMENTS,
        SUBENTITYLIST_ALLELEMENTSMUSTBEJOINED
    }

    private Merger() {
        throw new UnsupportedOperationException("freud.sql.merger.Merger cannot be instantiated.");
    }
    
	@SafeVarargs
	public static <EntityKey extends Comparable<EntityKey>> EntityKey[] keys(EntityKey ... keys) {
    	return (EntityKey[]) keys;
    }

    public static <MainEntity, SubEntity, EntityKeys extends Comparable<EntityKeys>> void join(
        MainEntity mainEntity,
        Function<MainEntity, EntityKeys[]> mainEntityKeyExtractor,
        List<SubEntity> subEntityList,
        Function<SubEntity, EntityKeys[]> subEntityKeyExtractor,
        Joiner<MainEntity, EntityKeys, SubEntity> joiner
    ) {
        var mainEntityList = Collections.singletonList(mainEntity);
        join(mainEntityList, mainEntityKeyExtractor, subEntityList, subEntityKeyExtractor, joiner);
    }

    public static <MainEntity, SubEntity, EntityKeys extends Comparable<EntityKeys>> void join(
            List<MainEntity> mainEntityList,
            Function<MainEntity, EntityKeys[]> mainEntityKeyExtractor,
            List<SubEntity> subEntityList,
            Function<SubEntity, EntityKeys[]> subEntityKeyExtractor,
            Joiner<MainEntity, EntityKeys, SubEntity> joiner
    ) {
        validateNotNull(mainEntityList, ErrorJoin.MAINENTITYLIST_MUSTBENOTNULL);
        validateNotNull(mainEntityKeyExtractor, ErrorJoin.MAINENTITYKEYEXTRACTOR_MUSTBENOTNULL);
        validateNotNull(subEntityList, ErrorJoin.SUBENTITYLIST_MUSTBENOTNULL);
        validateNotNull(subEntityKeyExtractor, ErrorJoin.SUBENTITYKEYEXTRACTOR_MUSTBENOTNULL);
        validateNotNull(joiner, ErrorJoin.JOINER_MUSTBENOTNULL);

        int mainEntityDataListSize = mainEntityList.size();
        int subEntityDataListSize = subEntityList.size();

        if (subEntityDataListSize == 0) {
            return;
        }

        validateNotEmpty(mainEntityList, ErrorJoin.MAINENTITYLIST_MUSTBENOTEMPTYWHENSUBENTITYLISTHASELEMENTS);

        final int keysLength = mainEntityKeyExtractor.apply(mainEntityList.get(0)).length;

        int mainEntityIndex = 0;
        int subEntityIndex = 0;
        while (subEntityIndex < subEntityDataListSize) {
            MainEntity mainEntity = mainEntityList.get(mainEntityIndex);
            SubEntity subEntity = subEntityList.get(subEntityIndex);

            int comparationResult = 0;

            final EntityKeys[] mainEntityKeysValues = mainEntityKeyExtractor.apply(mainEntity);
            final EntityKeys[] subEntityKeysValues = subEntityKeyExtractor.apply(subEntity);
            for (int keyIndex = 0; keyIndex < keysLength; keyIndex++) {
                var mainEntityKeyValue = mainEntityKeysValues[keyIndex];
                var subEntityKeyValue = subEntityKeysValues[keyIndex];

                int mainEntityKeyNullValue = mainEntityKeyValue == null ? 0 : 1;
                int subEntityKeyNullValue = subEntityKeyValue == null ? 0 : 1;
                comparationResult = mainEntityKeyNullValue - subEntityKeyNullValue;
                if (comparationResult != 0) {
                    break;
                }

                if ((mainEntityKeyValue == null) && (subEntityKeyValue == null)) {
                    continue;
                }

                comparationResult = mainEntityKeyValue.compareTo(subEntityKeyValue);
                if (comparationResult != 0) {
                    break;
                }
            }

            if (comparationResult == 0) {
                joiner.join(mainEntity, subEntityKeysValues, subEntity);
                subEntityIndex++;
                continue;
            }

            if (comparationResult < 0) {
                mainEntityIndex++;
                if (mainEntityIndex >= mainEntityDataListSize) {
                    break;
                }
                continue;
            }

            throw new EnumException(ErrorJoin.SUBENTITYLIST_MUSTHAVEALLMAINENTITYELEMENTS);
        }

        validate(subEntityIndex == subEntityDataListSize, ErrorJoin.SUBENTITYLIST_ALLELEMENTSMUSTBEJOINED);
    }
}
