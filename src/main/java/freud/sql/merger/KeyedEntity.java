package freud.sql.merger;

import static freud.err.Validations.validateNotEmpty;
import static freud.err.Validations.validateNotNull;

import java.util.Arrays;

public class KeyedEntity<Entity, EntityKey extends Comparable<EntityKey>> {

    public static enum ErrorNew {
        ENTITY_MUSTBENOTNULL, 
        KEYS_MUSTBENOTEMPTY
    }

    private Entity entity;
    private EntityKey[] keys;

    @SafeVarargs()
    public KeyedEntity(Entity entity, EntityKey ... keys) {
        validateNotNull(entity, ErrorNew.ENTITY_MUSTBENOTNULL);
        validateNotEmpty(keys, ErrorNew.KEYS_MUSTBENOTEMPTY);
        this.keys = keys;
        this.entity = entity;
    }

    public Entity getEntity() {
        return this.entity;
    }

    public EntityKey[] getKeys() {
        return this.keys;
    }

    @Override
    public String toString() {
        return Arrays.toString(this.keys) + "=" + this.entity.toString();
    }
}
