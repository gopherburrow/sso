package freud.sql;

import static freud.err.ExceptionTranslation.byEnum;
import static freud.err.Validations.validate;
import static freud.err.Validations.validateNot;
import static freud.err.Validations.validateNotEmpty;
import static freud.err.Validations.validateNotNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import javax.sql.DataSource;

import freud.err.EnumException;
import freud.err.Exceptions;

public class Sql {

  public static enum ErrorExecute {
    CONNECTION_MUSTBENOTNULL,
    SQL_MUSTBENOTEMPTY, 
    SQL_MUSTAFFECTROWS, 
    SQL_MUSTAFFECTSINGLEROW
  }

  public static enum ErrorExists {
    CONNECTION_MUSTBENOTNULL, 
    SQL_MUSTBENOTEMPTY
  }

  public static enum ErrorFind {
    CONNECTION_MUSTBENOTNULL, 
    SQL_MUSTBENOTEMPTY, 
    RESULTSETENTITYEXTRACTOR_MUSTBENOTNULL, 
    SQL_MUSTFOUNDARECORD,
    SQL_MUSTNOTFOUNDDUPLICATE
  }

  public static enum ErrorFindList {
    CONNECTION_MUSTBENOTNULL, 
    SQL_MUSTBENOTEMPTY, 
    RESULTSETENTITYEXTRACTOR_MUSTBENOTNULL
  }

  public static enum ErrorGetConnection {
    TRANSACTIONISOLATIONLEVEL_MUSTNOTNONEWHENAUTOCOMMITISFALSE,
    TRANSACTIONISOLATIONLEVEL_MUSTNOTBEREPEATABLEREADORSERIALIZABLEWHENAUTOCOMMITISTRUE
  }

  public static enum ErrorInsertSingleRecord {
    CONNECTION_MUSTBENOTNULL, 
    SQL_MUSTBENOTEMPTY, 
    SQL_MUSTAFFECTROWS, 
    SQL_MUSTAFFECTSINGLEROW
  }

  public static enum ErrorUpdateSingleRecord {
    CONNECTION_MUSTBENOTNULL, 
    SQL_MUSTBENOTEMPTY, 
    SQL_MUSTAFFECTROWS, 
    SQL_MUSTAFFECTSINGLEROW
  }

  public static enum ErrorDeleteSingleRecord {
    CONNECTION_MUSTBENOTNULL, 
    SQL_MUSTBENOTEMPTY, 
    SQL_MUSTAFFECTROWS, 
    SQL_MUSTAFFECTSINGLEROW
  }

  private Sql() {
    throw new UnsupportedOperationException("freud.sql.Sql cannot be instantiated.");
  }

  private static void fillPreparedStatementSqlParameters(PreparedStatement preparedStatement, Param... sqlParameters)
      throws SQLException {
    int sqlParameterLength = sqlParameters.length;
    for (int sqlParameterIndex = 0; sqlParameterIndex < sqlParameterLength; sqlParameterIndex++) {
      Param param = sqlParameters[sqlParameterIndex];
      preparedStatement.setObject(param.parameterIndex, param.value, param.sqlType);
    }
  }

  public static int execute(Connection connection, String sql, boolean checkSingleRecord, Param... sqlParameters) throws SQLException {
    validateNotNull(connection, ErrorExecute.CONNECTION_MUSTBENOTNULL);
    validateNotEmpty(sql, ErrorExecute.SQL_MUSTBENOTEMPTY);

    PreparedStatement preparedStatement = null;
    int rowsAffectedCount;
    try {
      preparedStatement = connection.prepareStatement(sql);
      fillPreparedStatementSqlParameters(preparedStatement, sqlParameters);
      rowsAffectedCount = preparedStatement.executeUpdate();

      if (checkSingleRecord) {
        if (rowsAffectedCount == 0) {
          throw new EnumException(ErrorExecute.SQL_MUSTAFFECTROWS);
        }

        if (rowsAffectedCount != 1) {
          throw new EnumException(ErrorExecute.SQL_MUSTAFFECTSINGLEROW);
        }
      }

    } finally {
      Sql.closeJdbcResources(preparedStatement);
    }
    return rowsAffectedCount;
  }

  public static int execute(Connection connection, String sql, Param... sqlParameters) throws SQLException {
    return execute(connection, sql, false, sqlParameters);
  }

  public static boolean exists(Connection connection, String sql, Param... sqlParameters) throws SQLException {
    validateNotNull(connection, ErrorExists.CONNECTION_MUSTBENOTNULL);
    validateNotEmpty(sql, ErrorExists.SQL_MUSTBENOTEMPTY);

    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    boolean found;
    try {
      preparedStatement = connection.prepareStatement(sql);
      fillPreparedStatementSqlParameters(preparedStatement, sqlParameters);

      resultSet = preparedStatement.executeQuery();

      found = resultSet.next();
    } finally {
      Sql.closeJdbcResources(resultSet, preparedStatement);
    }

    return found;
  }

  public static <Type> Type find(Connection connection, String sql, ResultSetEntityExtractor<Type> resultSetEntityExtractor, Param... sqlParameters) throws SQLException {
    validateNotNull(connection, ErrorFind.CONNECTION_MUSTBENOTNULL);
    validateNotEmpty(sql, ErrorFind.SQL_MUSTBENOTEMPTY);
    validateNotNull(resultSetEntityExtractor, ErrorFind.RESULTSETENTITYEXTRACTOR_MUSTBENOTNULL);

    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    Type type;
    try {
      preparedStatement = connection.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
      preparedStatement.setFetchSize(1);
      fillPreparedStatementSqlParameters(preparedStatement, sqlParameters);

      resultSet = preparedStatement.executeQuery();

      boolean found = resultSet.next();
      validate(found, ErrorFind.SQL_MUSTFOUNDARECORD);

      type = resultSetEntityExtractor.getEntity(resultSet);

      boolean foundDuplicate = resultSet.next();
      validateNot(foundDuplicate, ErrorFind.SQL_MUSTNOTFOUNDDUPLICATE);
    } finally {
      Sql.closeJdbcResources(resultSet, preparedStatement);
    }

    return type;
  }

  public static <Type> List<Type> findList(Connection connection, String sql, ResultSetEntityExtractor<Type> resultSetEntityExtractor, Param... sqlParameters) throws SQLException {
    validateNotNull(connection, ErrorFindList.CONNECTION_MUSTBENOTNULL);
    validateNotEmpty(sql, ErrorFindList.SQL_MUSTBENOTEMPTY);
    validateNotNull(resultSetEntityExtractor, ErrorFindList.RESULTSETENTITYEXTRACTOR_MUSTBENOTNULL);

    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    List<Type> typeList;
    try {
      preparedStatement = connection.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
      preparedStatement.setFetchSize(Short.MAX_VALUE);
      fillPreparedStatementSqlParameters(preparedStatement, sqlParameters);

      resultSet = preparedStatement.executeQuery();

      typeList = new ArrayList<>();
      while (resultSet.next()) {
        Type type = resultSetEntityExtractor.getEntity(resultSet);
        typeList.add(type);
      }
    } finally {
      Sql.closeJdbcResources(resultSet, preparedStatement);
    }

    return typeList;
  }

  public static Connection getConnection(DataSource dataSource, boolean autoCommit, boolean readOnly, int transactionIsolationLevel) throws SQLException {
    Connection connection = dataSource.getConnection();
    validateNot(!autoCommit && transactionIsolationLevel == Connection.TRANSACTION_NONE, ErrorGetConnection.TRANSACTIONISOLATIONLEVEL_MUSTNOTNONEWHENAUTOCOMMITISFALSE);
    validateNot(autoCommit && (transactionIsolationLevel == Connection.TRANSACTION_REPEATABLE_READ || transactionIsolationLevel == Connection.TRANSACTION_SERIALIZABLE), ErrorGetConnection.TRANSACTIONISOLATIONLEVEL_MUSTNOTBEREPEATABLEREADORSERIALIZABLEWHENAUTOCOMMITISTRUE);

    connection.setAutoCommit(autoCommit);
    connection.setReadOnly(readOnly);
    connection.setTransactionIsolation(transactionIsolationLevel);

    return connection;
  }

  public static int insert(Connection connection, String sql, Param... sqlParameters) throws SQLException {
    return execute(connection, sql, false, sqlParameters);
  }

  public static void insertSingleRecord(Connection connection, String sql, Param... sqlParameters) throws SQLException {
    Exceptions.translate(
      ()->execute(connection, sql, true, sqlParameters), 
        byEnum(ErrorExecute.CONNECTION_MUSTBENOTNULL, ErrorInsertSingleRecord.CONNECTION_MUSTBENOTNULL),
        byEnum(ErrorExecute.SQL_MUSTBENOTEMPTY, ErrorInsertSingleRecord.SQL_MUSTBENOTEMPTY),
        byEnum(ErrorExecute.SQL_MUSTAFFECTROWS, ErrorInsertSingleRecord.SQL_MUSTAFFECTROWS),
        byEnum(ErrorExecute.SQL_MUSTAFFECTSINGLEROW, ErrorInsertSingleRecord.SQL_MUSTAFFECTSINGLEROW)
    );
  }

  public static int update(Connection connection, String sql, Param... sqlParameters) throws SQLException {
    return execute(connection, sql, false, sqlParameters);
  }

  public static void updateSingleRecord(Connection connection, String sql, Param[] sqlParameters) throws SQLException {
    Exceptions.translate(
      ()->execute(connection, sql, true, sqlParameters), 
        byEnum(ErrorExecute.CONNECTION_MUSTBENOTNULL, ErrorUpdateSingleRecord.CONNECTION_MUSTBENOTNULL),
        byEnum(ErrorExecute.SQL_MUSTBENOTEMPTY, ErrorUpdateSingleRecord.SQL_MUSTBENOTEMPTY),
        byEnum(ErrorExecute.SQL_MUSTAFFECTROWS, ErrorUpdateSingleRecord.SQL_MUSTAFFECTROWS),
        byEnum(ErrorExecute.SQL_MUSTAFFECTSINGLEROW, ErrorUpdateSingleRecord.SQL_MUSTAFFECTSINGLEROW)
    );
  }


  public static int delete(Connection connection, String sql, Param... sqlParameters) throws SQLException {
    return execute(connection, sql, false, sqlParameters);
  }

  public static void deleteSingleRecord(Connection connection, String sql, Param... sqlParameters) throws SQLException {
    Exceptions.translate(
      ()->execute(connection, sql, true, sqlParameters), 
        byEnum(ErrorExecute.CONNECTION_MUSTBENOTNULL, ErrorDeleteSingleRecord.CONNECTION_MUSTBENOTNULL),
        byEnum(ErrorExecute.SQL_MUSTBENOTEMPTY, ErrorDeleteSingleRecord.SQL_MUSTBENOTEMPTY),
        byEnum(ErrorExecute.SQL_MUSTAFFECTROWS, ErrorDeleteSingleRecord.SQL_MUSTAFFECTROWS),
        byEnum(ErrorExecute.SQL_MUSTAFFECTSINGLEROW, ErrorDeleteSingleRecord.SQL_MUSTAFFECTSINGLEROW)
    );
  }

  public static Boolean getBoolean(ResultSet resultSet, int columnIndex) throws SQLException {
    Boolean result = Boolean.valueOf(resultSet.getBoolean(columnIndex));
    if (resultSet.wasNull()) {
      return null;
    }
    return result;
  }

  public static Boolean getBoolean(ResultSet resultSet, String columnLabel) throws SQLException {
    Boolean result = Boolean.valueOf(resultSet.getBoolean(columnLabel));
    if (resultSet.wasNull()) {
      return null;
    }
    return result;
  }

  public static Byte getByte(ResultSet resultSet, int columnIndex) throws SQLException {
    Byte result = Byte.valueOf(resultSet.getByte(columnIndex));
    if (resultSet.wasNull()) {
      return null;
    }
    return result;
  }

  public static Byte getByte(ResultSet resultSet, String columnLabel) throws SQLException {
    Byte result = Byte.valueOf(resultSet.getByte(columnLabel));
    if (resultSet.wasNull()) {
      return null;
    }
    return result;
  }

  public static Short getShort(ResultSet resultSet, int columnIndex) throws SQLException {
    Short result = Short.valueOf(resultSet.getShort(columnIndex));
    if (resultSet.wasNull()) {
      return null;
    }
    return result;
  }

  public static Short getShort(ResultSet resultSet, String columnLabel) throws SQLException {
    Short result = Short.valueOf(resultSet.getShort(columnLabel));
    if (resultSet.wasNull()) {
      return null;
    }
    return result;
  }

  public static Integer getInteger(ResultSet resultSet, String columnLabel) throws SQLException {
    Integer result = Integer.valueOf(resultSet.getInt(columnLabel));
    if (resultSet.wasNull()) {
      return null;
    }
    return result;
  }

  public static Integer getInteger(ResultSet resultSet, int columnIndex) throws SQLException {
    Integer result = Integer.valueOf(resultSet.getInt(columnIndex));
    if (resultSet.wasNull()) {
      return null;
    }
    return result;
  }

  public static Long getLong(ResultSet resultSet, int columnIndex) throws SQLException {
    Long result = Long.valueOf(resultSet.getLong(columnIndex));
    if (resultSet.wasNull()) {
      return null;
    }
    return result;
  }

  public static Long getLong(ResultSet resultSet, String columnLabel) throws SQLException {
    Long result = Long.valueOf(resultSet.getLong(columnLabel));
    if (resultSet.wasNull()) {
      return null;
    }
    return result;
  }

  public static Float getFloat(ResultSet resultSet, int columnIndex) throws SQLException {
    Float result = Float.valueOf(resultSet.getFloat(columnIndex));
    if (resultSet.wasNull()) {
      return null;
    }
    return result;
  }

  public static Float getFloat(ResultSet resultSet, String columnLabel) throws SQLException {
    Float result = Float.valueOf(resultSet.getFloat(columnLabel));
    if (resultSet.wasNull()) {
      return null;
    }
    return result;
  }

  public static Double getDouble(ResultSet resultSet, int columnIndex) throws SQLException {
    Double result = Double.valueOf(resultSet.getDouble(columnIndex));
    if (resultSet.wasNull()) {
      return null;
    }
    return result;
  }

  public static Double getDouble(ResultSet resultSet, String columnLabel) throws SQLException {
    Double result = Double.valueOf(resultSet.getDouble(columnLabel));
    if (resultSet.wasNull()) {
      return null;
    }
    return result;
  }

  public static LocalDate getLocalDate(ResultSet resultSet, int columnIndex, String zoneId) throws SQLException {
    Timestamp timestamp = resultSet.getTimestamp(columnIndex);
    Instant instant = Instant.ofEpochMilli(timestamp.getTime());
    return LocalDateTime.ofInstant(instant, ZoneId.of(zoneId)).toLocalDate();
  }

  public static LocalDate getLocalDate(ResultSet resultSet, String columnLabel, String zoneId) throws SQLException {
    Timestamp timestamp = resultSet.getTimestamp(columnLabel);
    Instant instant = Instant.ofEpochMilli(timestamp.getTime());
    return LocalDateTime.ofInstant(instant, ZoneId.of(zoneId)).toLocalDate();
  }

  public static LocalDate getLocalDate(ResultSet resultSet, int columnIndex) throws SQLException {
    return getLocalDate(resultSet, columnIndex, ZoneId.systemDefault().getId());
  }

  public static LocalDate getLocalDate(ResultSet resultSet, String columnLabel) throws SQLException {
    return getLocalDate(resultSet, columnLabel, ZoneId.systemDefault().getId());
  }

  public static LocalTime getLocalTime(ResultSet resultSet, String columnLabel, String zoneId) throws SQLException {
    Timestamp timestamp = resultSet.getTimestamp(columnLabel);
    Instant instant = Instant.ofEpochMilli(timestamp.getTime());
    return LocalDateTime.ofInstant(instant, ZoneId.of(zoneId)).toLocalTime();
  }

  public static LocalTime getLocalTime(ResultSet resultSet, int columnIndex, String zoneId) throws SQLException {
    Timestamp timestamp = resultSet.getTimestamp(columnIndex);
    Instant instant = Instant.ofEpochMilli(timestamp.getTime());
    return LocalDateTime.ofInstant(instant, ZoneId.of(zoneId)).toLocalTime();
  }

  public static LocalTime getLocalTime(ResultSet resultSet, int columnIndex) throws SQLException {
    return getLocalTime(resultSet, columnIndex, ZoneId.systemDefault().getId());
  }

  public static LocalTime getLocalTime(ResultSet resultSet, String columnLabel) throws SQLException {
    return getLocalTime(resultSet, columnLabel, ZoneId.systemDefault().getId());
  }

  public static LocalDateTime getLocalDateTime(ResultSet resultSet, int columnIndex, String zoneId)
      throws SQLException {
    Timestamp timestamp = resultSet.getTimestamp(columnIndex);
    Instant instant = Instant.ofEpochMilli(timestamp.getTime());
    return LocalDateTime.ofInstant(instant, ZoneId.of(zoneId));
  }

  public static LocalDateTime getLocalDateTime(ResultSet resultSet, String columnLabel, String zoneId)
      throws SQLException {
    Timestamp timestamp = resultSet.getTimestamp(columnLabel);
    Instant instant = Instant.ofEpochMilli(timestamp.getTime());
    return LocalDateTime.ofInstant(instant, ZoneId.of(zoneId));
  }

  public static LocalDateTime getLocalDateTime(ResultSet resultSet, int columnIndex) throws SQLException {
    return getLocalDateTime(resultSet, columnIndex, ZoneId.systemDefault().getId());
  }

  public static LocalDateTime getLocalDateTime(ResultSet resultSet, String columnLabel) throws SQLException {
    return getLocalDateTime(resultSet, columnLabel, ZoneId.systemDefault().getId());
  }

  public static <EnumType extends Enum<EnumType>, ColumnType> EnumType getEnum(Class<EnumType> enumType,
      Class<ColumnType> columnType, Function<ColumnType, EnumType> function, ResultSet resultSet, int columnIndex)
      throws SQLException {
    ColumnType enumDbValue = (ColumnType) resultSet.getObject(columnIndex, columnType);
    if (enumDbValue == null) {
      return null;
    }
    return function.apply(enumDbValue);
  }

  public static <EnumType extends Enum<EnumType>, ColumnType> EnumType getEnum(Class<EnumType> enumType,
      Class<ColumnType> columnType, Function<ColumnType, EnumType> function, ResultSet resultSet, String columnLabel)
      throws SQLException {
    ColumnType enumDbValue = (ColumnType) resultSet.getObject(columnLabel, columnType);
    if (enumDbValue == null) {
      return null;
    }
    return function.apply(enumDbValue);
  }

  public static <T extends Enum<T>> T getEnumFromName(Class<T> type, ResultSet resultSet, int columnIndex)
      throws SQLException {
    return getEnum(type, String.class, (enumDbValue) -> Enum.valueOf(type, enumDbValue), resultSet, columnIndex);
  }

  public static <T extends Enum<T>> T getEnumFromName(Class<T> type, ResultSet resultSet, String columnLabel)
      throws SQLException {
    return getEnum(type, String.class, (enumDbValue) -> {
      return Enum.valueOf(type, enumDbValue);
    }, resultSet, columnLabel);
  }

  public static <T extends Enum<T>> T getEnumFromOrdinal(Class<T> type, ResultSet resultSet, int columnIndex)
      throws SQLException {
    return getEnum(type, Integer.class, (enumDbValue) -> type.getEnumConstants()[enumDbValue.intValue()], resultSet,
        columnIndex);
  }

  public static <T extends Enum<T>> T getEnumFromOrdinal(Class<T> type, ResultSet resultSet, String columnLabel)
      throws SQLException {
    return getEnum(type, Integer.class, (enumDbValue) -> type.getEnumConstants()[enumDbValue.intValue()], resultSet,
        columnLabel);
  }

  public static void closeConnections(Connection... connections) throws SQLException {
    if (connections != null) {
      int connectionLength = connections.length;
      List<SQLException> sqlExceptionList = new ArrayList<>();
      for (int connectionIndex = 0; connectionIndex < connectionLength; connectionIndex++) {
        Connection connection = connections[connectionIndex];
        if (connection == null) {
          continue;
        }

        try {
          if (!connection.isClosed()) {
            connection.close();
          }
        } catch (SQLException e) {
          sqlExceptionList.add(e);
        }
      }

      launchComposedSqlException(sqlExceptionList);
    }
  }

  public static void tryCloseDataSource(DataSource dataSource) {
    Method closeMethod = null;
    try {
      closeMethod = dataSource.getClass().getMethod("close");
      closeMethod.invoke(dataSource);
    } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
    }
  }

  private static void closeJdbcResources(PreparedStatement preparedStatement) throws SQLException {
    closeJdbcResources(null, new PreparedStatement[] { preparedStatement });
  }

  private static void closeJdbcResources(ResultSet resultSet, PreparedStatement preparedStatement) throws SQLException {
    closeJdbcResources(new ResultSet[] { resultSet }, new PreparedStatement[] { preparedStatement });
  }

  private static void closeJdbcResources(ResultSet[] resultSets, PreparedStatement[] preparedStatements) throws SQLException {
    ArrayList<SQLException> sqlExceptionList = new ArrayList<SQLException>(3);

    if (resultSets != null) {
      int resultSetLength = resultSets.length;
      for (int resultSetIndex = 0; resultSetIndex < resultSetLength; resultSetIndex++) {
        ResultSet resultSet = resultSets[resultSetIndex];
        if (resultSet == null) {
          continue;
        }

        boolean isResultSetClosed;
        try {
          try {
            isResultSetClosed = resultSet.isClosed();
          } catch (Error e) {
            isResultSetClosed = false;
          }
          if (!isResultSetClosed) {
            resultSet.close();
          }
        } catch (SQLException e) {
          sqlExceptionList.add(e);
        }
      }
    }

    if (preparedStatements != null) {
      int preparedStatementLength = preparedStatements.length;
      for (int preparedStatementIndex = 0; preparedStatementIndex < preparedStatementLength; preparedStatementIndex++) {
        PreparedStatement preparedStatement = preparedStatements[preparedStatementIndex];
        if (preparedStatement == null) {
          continue;
        }

        boolean isPreparedStatementClosed;
        try {
          try {
            isPreparedStatementClosed = preparedStatement.isClosed();
          } catch (Error e) {
            isPreparedStatementClosed = false;
          }
          if (!isPreparedStatementClosed) {
            preparedStatement.close();
          }
        } catch (SQLException e) {
          sqlExceptionList.add(e);
        }
      }
    }

    launchComposedSqlException(sqlExceptionList);
  }

  private static void launchComposedSqlException(List<SQLException> sqlExceptionList) throws SQLException {
    if (sqlExceptionList.isEmpty()) {
      return;
    }

    StringBuilder exceptionMessageStringBuilder = new StringBuilder();
    for (SQLException sqlException : sqlExceptionList) {
      exceptionMessageStringBuilder.append("{");
      exceptionMessageStringBuilder.append(sqlException.getMessage());
      exceptionMessageStringBuilder.append("}");
    }

    String exceptionMessage = exceptionMessageStringBuilder.toString();
    SQLException composedSqlException = new SQLException(exceptionMessage);
    for (SQLException sqlException : sqlExceptionList) {
      composedSqlException.setNextException(sqlException);
    }

    throw composedSqlException;
  }

}
