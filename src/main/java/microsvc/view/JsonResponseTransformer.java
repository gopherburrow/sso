package microsvc.view;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonResponseTransformer {
	public static String render(Object model) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(model);
	}
}
