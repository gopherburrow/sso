package microsvc.view;

import io.micrometer.prometheus.PrometheusMeterRegistry;
import io.prometheus.client.exporter.common.TextFormat;
import spark.Request;
import spark.Response;
import spark.Service;

public class PrometheusView {
	private PrometheusMeterRegistry meterRegistry;

	public PrometheusView(PrometheusMeterRegistry meterRegistry, Service httpServer) {
		this.meterRegistry = meterRegistry;

		httpServer.get("/metrics", this::prometheusScrape);
	}

	public String prometheusScrape(Request req, Response res) throws Exception {
		res.status(200);
		res.type(TextFormat.CONTENT_TYPE_004);
		return this.meterRegistry.scrape();
	}
}
