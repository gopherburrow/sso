package microsvc.jwt;

import static freud.err.Exceptions.re;

import java.net.URL;
import java.net.URLConnection;

import org.jose4j.jwk.JsonWebKeySet;

import spark.utils.IOUtils;

public class JSONWebKeyStore {

	private JsonWebKeySet publicKeys;

	public JSONWebKeyStore(URL jwkUrl) {
		URLConnection connection = re(()->jwkUrl.openConnection());
		this.publicKeys = re(()->new JsonWebKeySet(IOUtils.toString(connection.getInputStream())));
	}
	public JsonWebKeySet getPublicKeys() {
		return publicKeys;
	}
}
