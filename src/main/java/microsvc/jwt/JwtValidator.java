package microsvc.jwt;

import static freud.err.ExceptionTranslation.by;
import static freud.err.Exceptions.re;

import java.util.Map;

import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.JsonWebKeySet;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jws.JsonWebSignatureAlgorithm;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.ErrorCodes;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.jwt.consumer.JwtContext;

import freud.err.EnumException;
import freud.err.Exceptions;

public class JwtValidator {
	public enum ErrorValidate { 
		JWT_MUSTBEVALID,	
		JWT_MUSTBENOTEXPIRED,
		JWT_MUSTHAVEVALIDSIGNATURE	
	}
	
	private JsonWebKeySet publicKeys;
	private JwtConsumer headerConsumer;

	public JwtValidator(JsonWebKeySet publicKeys) {
		this.publicKeys = publicKeys;
		this.headerConsumer = new JwtConsumerBuilder()
			.setSkipAllValidators()
			.setDisableRequireSignature()
			.setSkipSignatureVerification()
			.build();
	}

	public Map<String, Object> validate(String jwt) {
		JwtContext context = Exceptions.translate(()->this.headerConsumer.process(jwt), e->new EnumException(ErrorValidate.JWT_MUSTBEVALID, e));
		JsonWebSignature jwtHeader = (JsonWebSignature) context.getJoseObjects().stream().filter(t -> t instanceof JsonWebSignature).findFirst().get();
		JsonWebSignatureAlgorithm algorithm = re(()->jwtHeader.getAlgorithm());
		String kid = jwtHeader.getKeyIdHeaderValue();

		JsonWebKey jwk = this.publicKeys.findJsonWebKey(kid, algorithm.getKeyType(), null, algorithm.getAlgorithmIdentifier());

		JwtConsumer consumer = new JwtConsumerBuilder()
			.setSkipDefaultAudienceValidation()
			.setRequireExpirationTime() 
			.setAllowedClockSkewInSeconds(30) 
			.setVerificationKey(jwk.getKey())
			.build();
		
		JwtClaims extractedClaims = Exceptions.translate(()->consumer.processToClaims(jwt),
			by(e->e instanceof InvalidJwtException && ((InvalidJwtException)e).hasErrorCode(ErrorCodes.EXPIRED), e-> new EnumException(ErrorValidate.JWT_MUSTBENOTEXPIRED, e)),
			by(e->e instanceof InvalidJwtException && ((InvalidJwtException)e).hasErrorCode(ErrorCodes.SIGNATURE_INVALID), e-> new EnumException(ErrorValidate.JWT_MUSTHAVEVALIDSIGNATURE, e))
		);
		
		return extractedClaims.getClaimsMap();
	}
}
