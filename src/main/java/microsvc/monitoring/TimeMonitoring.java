package microsvc.monitoring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Supplier;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.Timer.Sample;

public class TimeMonitoring {
	private MeterRegistry meterRegistry;
	private String[] tags;

	public TimeMonitoring(MeterRegistry meterRegistry, String ... tags) {
		super();
		this.meterRegistry = meterRegistry;
		this.tags = tags;
	}

	public <Result> Result getOn(Supplier<Result> call, String ... tags) {
		var concatTags = new ArrayList<String>(Arrays.asList(this.tags));
		concatTags.addAll(Arrays.asList(tags));
		var tagArrays = concatTags.<String>toArray(s->new String[s]);
        Sample sample = Timer.start(this.meterRegistry);
		try {
			return call.get();
		} finally {
			sample.stop(this.meterRegistry.timer("microsvc.monitoring.TimeMonitoring", tagArrays));
		}
	}

	public void on(Runnable call, String ... tags) {
		var concatTags = new ArrayList<String>(Arrays.asList(this.tags));
		concatTags.addAll(Arrays.asList(tags));
		var tagArrays = concatTags.<String>toArray(s->new String[s]);
        Sample sample = Timer.start(this.meterRegistry);
		try {
			call.run();
		} finally {
			sample.stop(this.meterRegistry.timer("microsvc.monitoring.TimeMonitoring", tagArrays));
		}
	}

}
