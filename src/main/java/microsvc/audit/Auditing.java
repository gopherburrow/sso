package microsvc.audit;

import static freud.err.Exceptions.re;

import java.util.TreeMap;
import java.util.function.Supplier;

import com.fasterxml.jackson.databind.ObjectMapper;

import freud.err.EnumException;

public class Auditing {
	public static final String RESULT_OK = "OK";
	public static final String RESULT_FAIL = "FAIL";
	public static final String RESULT_ERROR = "ERROR";
	
    public Auditing() {
    	super();
	}
    
    public void auditOperation(String name, String result, String message, String ... params) {
    	var logMap = new TreeMap<String, Object>();
    	logMap.put("name", name);
    	logMap.put("result", result);
    	logMap.put("message", message);
		logMap.put("params", params);
		var logString = re(() -> new ObjectMapper().writeValueAsString(logMap));
		System.out.println(logString);
    }
    
	public <T> T getOnAudit(Supplier<T> call, String operation, String ... callParams) {
		try {
			T result = call.get();
			this.auditOperation(operation, RESULT_OK, null, callParams);
			return result;
		} catch (EnumException e) {
			this.auditOperation(operation, RESULT_FAIL, e.value.name(), callParams);
			throw e;
		} catch (RuntimeException e) {
			this.auditOperation(operation, RESULT_ERROR, getRootCauseMessage(e), callParams);
			throw e;
		}
	}

	public void onAudit(Runnable call, String operation, String ... callParams) {
		try {
			call.run();
			this.auditOperation(operation, RESULT_OK, null, callParams);
			return;
		} catch (EnumException e) {
			this.auditOperation(operation, RESULT_FAIL, e.value.name(), callParams);
			throw e;
		} catch (RuntimeException e) {
			this.auditOperation(operation, RESULT_ERROR, getRootCauseMessage(e), callParams);
			throw e;
		}
	}
	
	public static String getRootCauseMessage(Throwable e) {
		var eLoop = e;
		while (eLoop.getCause() != eLoop && eLoop.getCause() != null) {
			eLoop = eLoop.getCause();
		}
		return eLoop.getClass().getSimpleName() + ":" + eLoop.getMessage();
	}

}
