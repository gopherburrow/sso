package microsvc.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class Error implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public static class Links {
		@JsonInclude(Include.NON_EMPTY)	public String about;
	}

	public static class Source {
		@JsonInclude(Include.NON_EMPTY)	public String pointer;
		@JsonInclude(Include.NON_EMPTY)	public String parameter;
	}

	@JsonInclude(Include.NON_EMPTY) public String id;
	@JsonInclude(Include.NON_EMPTY) public Links links;
	@JsonInclude(Include.NON_EMPTY) public int status;
	@JsonInclude(Include.NON_EMPTY) public String code;
	@JsonInclude(Include.NON_EMPTY) public String title;
	@JsonInclude(Include.NON_EMPTY) public String detail;
	@JsonInclude(Include.NON_EMPTY) public Source source;
	@JsonInclude(Include.NON_EMPTY) public String meta;

	public Error(String id, Links links, int status, String code, String title, String detail, Source source, String meta) {
		super();
		this.id = id;
		this.links = links;
		this.status = status;
		this.code = code;
		this.title = title;
		this.detail = detail;
		this.source = source;
		this.meta = meta;
	}

	public Error(int status, String code, String title) {
		this(null, null, status, code, title, null, null, null);
	}
}
