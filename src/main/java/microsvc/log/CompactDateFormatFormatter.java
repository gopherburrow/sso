package microsvc.log;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class CompactDateFormatFormatter extends Formatter {
	private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS").withZone(ZoneId.systemDefault());

	@Override
	public String format(LogRecord record) {
		return this.dateTimeFormatter.format(record.getInstant()) + ":" + record.getLevel().getName() + ":" + record.getMessage() + "\n";
	}
}
