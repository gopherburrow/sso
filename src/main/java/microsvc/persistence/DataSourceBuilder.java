package microsvc.persistence;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;

public class DataSourceBuilder {
    private static final int POOL_MIN_POOL_SIZE = 1;
    private static final long POOL_IDLE_TIMEOUT = MINUTES.toMillis(10);
    private static final int POOL_MAX_POOL_SIZE = 10;
    private static final long POOL_MAX_LIFETIME = MINUTES.toMillis(30);
    private static final long POOL_CONNECTION_TIMEOUT = SECONDS.toMillis(30);
    private static final long POOL_VALIDATION_TIMEOUT = SECONDS.toMillis(5);

    public String url;
    public String srvType;
    public String username;
    public String password;
    public int minimumPoolIdle = POOL_MIN_POOL_SIZE;
    public long idleTimeoutMs = POOL_IDLE_TIMEOUT;
    public int maxPoolSize = POOL_MAX_POOL_SIZE;
    public long maxLifetimeMs = POOL_MAX_LIFETIME;
    public long connectionTimeoutMs = POOL_CONNECTION_TIMEOUT;
    public long validationTimeoutMs = POOL_VALIDATION_TIMEOUT;
    public Object metricRegistry;

    public DataSourceBuilder(
            String url,
            String databaseUsername,
            String databasePassword,
            Object metricRegistry
    ) {
        super();
        this.url = url;
        this.username = databaseUsername;
        this.password = databasePassword;
        this.metricRegistry = metricRegistry;
    }

    public DataSource build() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(this.url);
        config.setUsername(this.username);
        config.setPassword(this.password);
        
        config.setMinimumIdle(this.minimumPoolIdle);
        config.setMaximumPoolSize(this.maxPoolSize);
        config.setMaxLifetime(this.maxLifetimeMs);
        config.setConnectionTimeout(this.connectionTimeoutMs);
        config.setValidationTimeout(this.validationTimeoutMs);
        config.setIdleTimeout(this.idleTimeoutMs);
        config.setMetricRegistry(this.metricRegistry);
        config.setAutoCommit(false);
        return new HikariDataSource(config);
    }
}
