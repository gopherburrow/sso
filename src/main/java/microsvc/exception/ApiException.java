package microsvc.exception;

public class ApiException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public int httpStatus;
	public String code;
	public String detail;
	public String linksAboutUrl;
	public String sourcePointer;
	public String sourceParameter;

	public ApiException(int httpStatus, String code, String title, Throwable cause, String detail, String linksAboutUrl, String sourcePointer, String sourceParameter) {
		super(title, cause, true, false);
		this.httpStatus = httpStatus;
		this.code = code;
		this.detail = detail;
		this.linksAboutUrl = linksAboutUrl;
		this.sourcePointer = sourcePointer;
		this.sourceParameter = sourceParameter;
	}

	public ApiException(int httpStatus, String code, String title, Throwable cause) {
		this(httpStatus, code, title, cause, null, null, null, null);
	}
}
