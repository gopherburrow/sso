package microsvc.exception;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import freud.err.EnumException;
import freud.err.ExceptionTranslation;
import freud.msg.I18n;
import microsvc.dto.Error;
import microsvc.dto.Errors;
import spark.Request;
import spark.Response;

public class ApiExceptionHandler {
	
	private static final String ERR_NEW_UNSUPPORTED = "microsvc.exception.ApiExceptionHandler.new.unsupported";

	private ApiExceptionHandler() {
		throw new UnsupportedOperationException(ERR_NEW_UNSUPPORTED);
	}
	
	private static ObjectMapper mapper = new ObjectMapper();;

	public static void handle(ApiException e, Request request, Response response) {
		Errors errors = new Errors(
			new Error(e.httpStatus,e.code,I18n.getString(e.getMessage()))
		);
		response.status(e.httpStatus);
		response.header("Content-Type", "application/json");
		try {
			response.body(mapper.writeValueAsString(errors));
		} catch (JsonProcessingException e1) {
			response.body("Internal Server Error");
		}
	}
	
	public static ExceptionTranslation toForbidden(Enum<?> enumValue) {
		return ExceptionTranslation.byEnum(enumValue, e->new ApiException(HttpServletResponse.SC_FORBIDDEN, ((EnumException)e).value.name(), I18n.getString(e.getMessage()), e));
	}

	public static ExceptionTranslation toUnauthorized(Enum<?> enumValue) {
		return ExceptionTranslation.byEnum(enumValue, e->new ApiException(HttpServletResponse.SC_UNAUTHORIZED, ((EnumException)e).value.name(), I18n.getString(e.getMessage()), e));
	}
	
	public static ExceptionTranslation toNotFound(Enum<?> enumValue) {
		return ExceptionTranslation.byEnum(enumValue, e->new ApiException(HttpServletResponse.SC_NOT_FOUND, ((EnumException)e).value.name(), I18n.getString(e.getMessage()), e));
	}

}
