package microsvc.exception;

public class ViewException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	public int httpStatus;
	public String view;
	public Object model;

	public ViewException(int httpStatus, Object model, String view, Throwable cause) {
		super(cause.getMessage(), cause, true, false);
		this.httpStatus = httpStatus;
		this.model = model;
		this.view = view;
	}
}
