package microsvc.exception;

import java.util.Collections;

import javax.servlet.http.HttpServletResponse;

import freud.err.ExceptionTranslation;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.template.mustache.MustacheTemplateEngine;

public class ViewExceptionHandler {
	
	private static final String TEMPLATES_LOCATION = "templates";
	private static final String TEMPLATE_ERROR_HTML = "error.html";

	private static final MustacheTemplateEngine TEMPLATES = new MustacheTemplateEngine(TEMPLATES_LOCATION);
	
	private static final String ERR_NEW_UNSUPPORTED = "microsvc.exception.ViewExceptionHandler.new.unsupported";

	private ViewExceptionHandler() {
		throw new UnsupportedOperationException(ERR_NEW_UNSUPPORTED);
	}
	
	public static void handle(ViewException e, Request request, Response response) {
		response.status(e.httpStatus);
		response.type("text/html;charset=utf-8");
		response.body(TEMPLATES.render(new ModelAndView(e.model, e.view)));
	}
	
	public static ExceptionTranslation toForbidden(Enum<?> enumValue) {
		return ExceptionTranslation.byEnum(enumValue, e->new ViewException(HttpServletResponse.SC_FORBIDDEN, Collections.singletonMap("code", e.getMessage()), TEMPLATE_ERROR_HTML, e));
	}

	public static ExceptionTranslation toUnauthorized(Enum<?> enumValue) {
		return ExceptionTranslation.byEnum(enumValue, e->new ViewException(HttpServletResponse.SC_UNAUTHORIZED, Collections.singletonMap("code", e.getMessage()), TEMPLATE_ERROR_HTML, e));
	}
	
	public static ExceptionTranslation toNotFound(Enum<?> enumValue) {
		return ExceptionTranslation.byEnum(enumValue, e->new ViewException(HttpServletResponse.SC_NOT_FOUND, Collections.singletonMap("code", e.getMessage()), TEMPLATE_ERROR_HTML, e));
	}

}
