package microsvc.exception;

import static freud.err.Exceptions.stackTraceToString;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import spark.Request;
import spark.Response;

public class DefaultExceptionHandler {
	
	private static final String ERR_NEW_UNSUPPORTED = "microsvc.exception.DefaultExceptionHandler.new.unsupported";

	private DefaultExceptionHandler() {
		throw new UnsupportedOperationException(ERR_NEW_UNSUPPORTED);
	}
	
	public static void handle(Exception e, Request request, Response response) {
		String stackTrace = stackTraceToString(e);
		Logger.getGlobal().log(Level.SEVERE, stackTrace, e);
		response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		response.body("Internal Server Error");
	}
}
