package sso.log;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import io.sentry.Sentry;
import io.sentry.jul.SentryHandler;
import microsvc.log.CompactDateFormatFormatter;

public class LogConfig {
	private static final String ROOT_LOGGER_NAME = "";
	
	// private Level getLevelFromSlf4jLevel(String slf4jLevelName) {
	// 	if(slf4jLevelName == null) {
	// 		return Level.SEVERE;
	// 	}
	// 	switch (org.slf4j.event.Level.valueOf(slf4jLevelName)) {
	// 		case DEBUG:
	// 			return Level.FINE;
	// 		case ERROR:
	// 			return Level.SEVERE;
	// 		case INFO:
	// 			return Level.INFO;
	// 		case TRACE:
	// 			return Level.FINEST;
	// 		case WARN:
	// 			return Level.WARNING;
	// 		default:
	// 			return Level.SEVERE;
	// 	}
	// }

	public LogConfig() {
    	var logger = Logger.getLogger(ROOT_LOGGER_NAME);
    	logger.setLevel(Level.INFO);
    	logger.setFilter((logRecord)->true);
    	var consoleHandler = new ConsoleHandler();
		consoleHandler.setFormatter(new CompactDateFormatFormatter());
		logger.addHandler(consoleHandler);
		
		Sentry.init();
		SentryHandler sentryHandler = new SentryHandler();
		sentryHandler.setLevel(Level.INFO);
		sentryHandler.setFilter((logRecord)->logRecord.getSourceClassName().startsWith("sso.") || logRecord.getSourceClassName().startsWith("microsvc.") || logRecord.getSourceClassName().startsWith("br.gov.serpro."));
		logger.addHandler(sentryHandler);
	}
	
}
