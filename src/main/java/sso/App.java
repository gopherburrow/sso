package sso;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import freud.err.Exceptions;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import microsvc.audit.Auditing;
import microsvc.exception.ApiException;
import microsvc.exception.ApiExceptionHandler;
import microsvc.exception.DefaultExceptionHandler;
import microsvc.exception.ViewException;
import microsvc.exception.ViewExceptionHandler;
import microsvc.monitoring.MeterRegistryBuilder;
import microsvc.persistence.DataSourceBuilder;
import microsvc.view.JsonResponseTransformer;
import microsvc.view.PrometheusView;
import oauth2.exception.OAuth2AuthorizeErrorException;
import oauth2.exception.OAuth2AuthorizeErrorExceptionHandler;
import sso.log.LogConfig;

public class App {

	public static final String APP_NAME = "SSO";

	private static final int MAIN_SERVER_HTTP_PORT = 8080;
	private static final int MONITORING_SERVER_HTTP_PORT = 1234;

	private PrometheusMeterRegistry meterRegistry;

	private DataSource dataSource;
	private Persistence persistence;

	private Auditing auditing;
	private Service service;
	private spark.Service mainSrv;
	private spark.Service monitoringSrv;
	@SuppressWarnings("unused")
	private View view;
	@SuppressWarnings("unused")
	private PrometheusView prometheusView;

	public static void main(String[] args) throws SecurityException, IOException {
		System.setProperty("java.util.logging.config.class", LogConfig.class.getName());
		Exceptions.setLogIgnore(e -> Logger.getGlobal().log(Level.INFO, "Ignoring Exception: " + e.getMessage()));
		@SuppressWarnings("unused")
		App app = new App();
	}

	public App() {
		try {
			this.meterRegistry = (new MeterRegistryBuilder()).build();

			this.dataSource = (new DataSourceBuilder("jdbc:postgresql://localhost:5432/sso", "sso", "sso", this.meterRegistry)).build();
			this.persistence = new Persistence(this.dataSource, this.meterRegistry, 180);

			this.auditing = new Auditing();
			this.service = new Service(this.persistence, auditing, this.getClass().getResource("/keystore.jwks")); //TODO JWK URL
			this.mainSrv = spark.Service.ignite().port(MAIN_SERVER_HTTP_PORT).threadPool(10);
			this.mainSrv.initExceptionHandler(e -> {
				Logger.getGlobal().log(Level.SEVERE, Exceptions.stackTraceToString(e), e);
				System.exit(100);
			});
			this.mainSrv.before((request, response) -> response.type("application/json"));
			this.mainSrv.defaultResponseTransformer(JsonResponseTransformer::render);
			this.mainSrv.exception(OAuth2AuthorizeErrorException.class, OAuth2AuthorizeErrorExceptionHandler::handle);
			this.mainSrv.exception(ViewException.class, ViewExceptionHandler::handle);
			this.mainSrv.exception(ApiException.class, ApiExceptionHandler::handle);
			this.mainSrv.exception(Exception.class, DefaultExceptionHandler::handle);

			this.view = new View(this.service, this.mainSrv);

			this.monitoringSrv = spark.Service.ignite().port(MONITORING_SERVER_HTTP_PORT).threadPool(4, 1, (int) SECONDS.toMillis(30));
			this.monitoringSrv.initExceptionHandler(e -> {
				Logger.getGlobal().log(Level.SEVERE, Exceptions.stackTraceToString(e), e);
				System.exit(100);
			});
			this.monitoringSrv.exception(Exception.class, DefaultExceptionHandler::handle);
			this.prometheusView = new PrometheusView(this.meterRegistry, this.monitoringSrv);
			Logger.getGlobal().info("Prometheus Enabled");
			Logger.getGlobal().log(Level.INFO, "App UP");
		} catch (Exception e) {
			Logger.getGlobal().log(Level.SEVERE, Exceptions.stackTraceToString(e), e);
		}
	}
}
