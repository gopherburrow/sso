package sso;

public interface KeySelector<T> {
	public SelectedKey getSelectedKey(T t);
}
