package sso;

import static freud.http.Headers.xForwardedFor;
import static microsvc.exception.ViewExceptionHandler.toForbidden;
import static oauth2.exception.OAuth2AuthorizeErrorExceptionHandler.toOAuth2AuthorizeError;

import freud.err.Exceptions;
import freud.http.Headers;
import freud.web.UrlEditor;
import oauth2.AuthorizeEndpoint;
import oauth2.TokenEndpoint;
import spark.Request;
import spark.Response;
import sso.Service.ErrorAuthorize;
import sso.entity.AuthorizationCodeGrantResponse;
import sso.entity.TokenResponse;

public class View {
	
	private Service service;

	public View(Service service, spark.Service httpServer) {
		this.service = service;
	
		httpServer.get("/authorize", this::getAuthorize);
		httpServer.post("/token", this::postToken);
	}
	
    public Void getAuthorize(Request req, Response res) throws Exception{
    	var redirectUri = req.queryParams(AuthorizeEndpoint.REQUEST_PARAMETER_REDIRECT_URI); //Spark Decode URLs for free
    	var state = req.queryParams(AuthorizeEndpoint.REQUEST_PARAMETER_STATE);
    	var authorizeResponse = Exceptions.translate(
			()->this.service.authorize(
				req.ip(), 
				xForwardedFor(req), 
				req.cookie("Session_GovBr"), 
				req.queryParams(AuthorizeEndpoint.REQUEST_PARAMETER_CLIENT_ID), 
				req.queryParams(AuthorizeEndpoint.REQUEST_PARAMETER_RESPONSE_TYPE), 
				redirectUri, 
				req.queryParams(AuthorizeEndpoint.REQUEST_PARAMETER_SCOPE), 
				state,
				req.queryParams(AuthorizeEndpoint.PKCE_REQUEST_PARAMETER_CODE_CHALLENGE),
				req.queryParams(AuthorizeEndpoint.PKCE_REQUEST_PARAMETER_CODE_CHALLENGE_METHOD),
				req.queryParams(AuthorizeEndpoint.OPENID_REQUEST_PARAMETER_NONCE),
				req.queryParams(AuthorizeEndpoint.OPENID_REQUEST_PARAMETER_DISPLAY),
				req.queryParams(AuthorizeEndpoint.OPENID_REQUEST_PARAMETER_PROMPT),
				req.queryParams(AuthorizeEndpoint.OPENID_REQUEST_PARAMETER_MAX_AGE),
				req.queryParams(AuthorizeEndpoint.OPENID_REQUEST_PARAMETER_UI_LOCALES),
				req.queryParams(AuthorizeEndpoint.OPENID_REQUEST_PARAMETER_ID_TOKEN_HINT),
				req.queryParams(AuthorizeEndpoint.OPENID_REQUEST_PARAMETER_LOGIN_HINT),
				req.queryParams(AuthorizeEndpoint.OPENID_REQUEST_PARAMETER_ACR_VALUES),
				req.queryParams(AuthorizeEndpoint.OPENID_REQUEST_PARAMETER_REQUEST)
			),
			//The Errors translations that cannot be reported back need be strictly ordered as below...
			toForbidden(ErrorAuthorize.CLIENTID_MUSTBENOTEMPTY),
			toForbidden(ErrorAuthorize.REDIRECTURI_MUSTBENOTEMPTYWHENSCOPECONTAINSOPENID),
			toForbidden(ErrorAuthorize.REDIRECTURI_MUSTBEURL),
			toForbidden(ErrorAuthorize.REDIRECTURI_MUSTBEABSOLUTEURL),
			toForbidden(ErrorAuthorize.REDIRECTURI_MUSTBESECUREURL),
			toForbidden(ErrorAuthorize.REDIRECTURI_MUSTNOTHAVEFRAGMENT),
			toForbidden(ErrorAuthorize.CLIENTID_MUSTEXISTS),
			toForbidden(ErrorAuthorize.REDIRECTURI_MUSTBENOTEMPTYWHENCLIENTHASMULTIPLEREDIRECTURIS),
			toForbidden(ErrorAuthorize.REDIRECTURI_MUSTBECLIENTREGISTERED),
			//Until here.
			toOAuth2AuthorizeError(ErrorAuthorize.RESPONSETYPE_MUSTBENOTEMPTY, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST),
			toOAuth2AuthorizeError(ErrorAuthorize.RESPONSETYPE_MUSTBESUPPORTED, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_UNSUPPORTED_RESPONSE_TYPE),
			toOAuth2AuthorizeError(ErrorAuthorize.RESPONSETYPE_MUSTBECLIENTSUPPORTED, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_UNAUTHORIZED_CLIENT),
			toOAuth2AuthorizeError(ErrorAuthorize.SCOPE_MUSTBECLIENTSUPPORTED, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_INVALID_SCOPE),
			toOAuth2AuthorizeError(ErrorAuthorize.CODECHALLENGE_CLIENTMUSTALLOWPKCE, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST),
			toOAuth2AuthorizeError(ErrorAuthorize.CODECHALLENGEMETHOD_CLIENTMUSTALLOWPKCE, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST),
			toOAuth2AuthorizeError(ErrorAuthorize.CODECHALLENGEMETHOD_MUSTBESUPPORTED, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST),
			toOAuth2AuthorizeError(ErrorAuthorize.CODECHALLENGE_CLIENTREQUIRESPKCE, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST),
			toOAuth2AuthorizeError(ErrorAuthorize.CODECHALLENGEMETHOD_CLIENTREQUIRESSTRONGPKCE, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST),
			toOAuth2AuthorizeError(ErrorAuthorize.DISPLAY_MUSTBESUPPORTED, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST),
			toOAuth2AuthorizeError(ErrorAuthorize.PROMPT_MUSTBESUPPORTED, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST),
			toOAuth2AuthorizeError(ErrorAuthorize.MAXAGE_MUSTBESUPPORTED, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST),
			toOAuth2AuthorizeError(ErrorAuthorize.UILOCALES_MUSTBESUPPORTED, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST),
			toOAuth2AuthorizeError(ErrorAuthorize.IDTOKENHINT_MUSTBESUPPORTED, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST),
			toOAuth2AuthorizeError(ErrorAuthorize.LOGINHINT_MUSTBESUPPORTED, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST),
			toOAuth2AuthorizeError(ErrorAuthorize.ACRVALUES_MUSTBESUPPORTED, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_INVALID_REQUEST),
			toOAuth2AuthorizeError(ErrorAuthorize.REQUEST_MUSTBESUPPORTED, AuthorizeEndpoint.OPENID_ERROR_RESPONSE_PARAMETER_ERROR_REQUEST_NOT_SUPPORTED)
			//toOAuthError(CODEJWTSIGNATUREKEY_MUSTBEFOUNDINKEYSTORE)
			//ExceptionTranslation.any(e->new OAuth2AuthorizeErrorException(redirectUri, AuthorizeEndpoint.ERROR_RESPONSE_PARAMETER_ERROR_SERVER_ERROR, I18n.getString(e.getMessage()), null, state, e))
		);
    	if(authorizeResponse instanceof AuthorizationCodeGrantResponse) {
    		var authorizationCodeGrantResponse = (AuthorizationCodeGrantResponse) authorizeResponse;
    		UrlEditor redirectUriEditor = new UrlEditor(authorizationCodeGrantResponse.redirect_uri);
			redirectUriEditor.setQueryParameterValues(AuthorizeEndpoint.RESPONSE_PARAMETER_CODE, authorizationCodeGrantResponse.code);
			if (authorizationCodeGrantResponse.state != null && !authorizationCodeGrantResponse.state.isEmpty()) {
				redirectUriEditor.setQueryParameterValues(AuthorizeEndpoint.RESPONSE_PARAMETER_STATE, authorizationCodeGrantResponse.state);
			}
    		res.redirect(redirectUriEditor.toUrlString());
    	}
    	return null;
    }
    
    public TokenResponse postToken(Request req, Response res) throws Exception{
    	var result = Exceptions.translate(
    		()-> { 
				//TODO
	    		UrlEditor formBodyEditor = new UrlEditor("");
	    		formBodyEditor.setQueryParameters(req.body());
				return this.service.token(
					req.ip(), 
					xForwardedFor(req), 
					Headers.authorizationBasic(req), 
					formBodyEditor.getQueryParameterValue(TokenEndpoint.REQUEST_FORM_PARAMETER_GRANT_TYPE), 
					formBodyEditor.getQueryParameterValue(TokenEndpoint.REQUEST_FORM_PARAMETER_CODE), 
					formBodyEditor.getQueryParameterValue(TokenEndpoint.REQUEST_FORM_PARAMETER_REDIRECT_URI), 
					formBodyEditor.getQueryParameterValue(TokenEndpoint.REQUEST_FORM_PARAMETER_CLIENT_ID),
					formBodyEditor.getQueryParameterValue(TokenEndpoint.REQUEST_FORM_PARAMETER_CODE_VERIFIER)
				);
    		}
		);
    	res.header("Cache-Control", "no-store"); 	
    	res.header("Cache-Pragma", "no-cache"); 	
    	return result;
    }
}
