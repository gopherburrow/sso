package sso;

import static freud.err.ExceptionTranslation.byEnum;
import static freud.err.Exceptions.re;
import static freud.err.Validations.validate;
import static freud.err.Validations.validateNot;
import static freud.err.Validations.validateNotEmpty;
import static freud.err.Validations.validateNotNull;
import static freud.err.Validations.validateNull;
import static freud.err.Validations.validateType;
import static freud.tx.TransactionIsolationLevel.READ_COMMITED;
import static freud.tx.TransactionType.READONLY;
import static freud.tx.Transactions.getOnTx;
import static java.util.Arrays.asList;
import static java.util.Arrays.binarySearch;

import java.net.URL;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Base64;
import java.util.Map;

import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.JsonWebKeySet;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.NumericDate;
import org.jose4j.keys.HmacKey;

import freud.err.EnumException;
import freud.err.Exceptions;
import freud.web.UrlEditor;
import microsvc.audit.Auditing;
import microsvc.jwt.JSONWebKeyStore;
import microsvc.jwt.JwtValidator;
import oauth2.AuthorizationCodeClaims;
import oauth2.AuthorizeEndpoint;
import oauth2.TokenEndpoint;
import sso.entity.AuthorizationCodeGrantResponse;
import sso.entity.Client;
import sso.entity.Client.PkceConfig;
import sso.entity.Client.TokenEndpointAuthType;
import sso.entity.TokenResponse;
import sso.entity.UsernamePasswordCredentials;

public class Service {

	private static final int CODE_JWT_EXP_DURATION = 10;

	public static enum ErrorNew {
		PERSISTENCE_MUSTBENOTNULL, AUDITING_MUSTBENOTNULL
	}

	public static enum ErrorAuthorize {
		// Authentication errors
		SESSIONTOKEN_MUSTBENOTEMPTY, // TODO SESSION_TOKEN Other Validations

		// Standard OAuth2 Errors, Cannot report back.
		CLIENTID_MUSTBENOTEMPTY, 
		REDIRECTURI_MUSTBENOTEMPTYWHENSCOPECONTAINSOPENID, 
		REDIRECTURI_MUSTBEURL,
		REDIRECTURI_MUSTBEABSOLUTEURL, 
		REDIRECTURI_MUSTBESECUREURL, 
		REDIRECTURI_MUSTNOTHAVEFRAGMENT,
		CLIENTID_MUSTEXISTS, 
		REDIRECTURI_MUSTBENOTEMPTYWHENCLIENTHASMULTIPLEREDIRECTURIS,
		REDIRECTURI_MUSTBECLIENTREGISTERED,

		// Standard OAuth2 Errors, Will report back.
		RESPONSETYPE_MUSTBENOTEMPTY, 
		RESPONSETYPE_MUSTBESUPPORTED, 
		RESPONSETYPE_MUSTBECLIENTSUPPORTED,
		SCOPE_MUSTBECLIENTSUPPORTED,

		// Infrasctructure Error
		CODEJWTSIGNATUREKEY_MUSTBEFOUNDINKEYSTORE,

		// OAuth2 PKCE Errors
		CODECHALLENGE_CLIENTMUSTALLOWPKCE, 
		CODECHALLENGEMETHOD_CLIENTMUSTALLOWPKCE, 
		CODECHALLENGEMETHOD_MUSTBESUPPORTED,
		CODECHALLENGE_CLIENTREQUIRESPKCE, 
		CODECHALLENGEMETHOD_CLIENTREQUIRESSTRONGPKCE,

		// OpenID Connect unsupported features
		DISPLAY_MUSTBESUPPORTED, 
		PROMPT_MUSTBESUPPORTED, 
		MAXAGE_MUSTBESUPPORTED, 
		UILOCALES_MUSTBESUPPORTED,
		IDTOKENHINT_MUSTBESUPPORTED, 
		LOGINHINT_MUSTBESUPPORTED, 
		ACRVALUES_MUSTBESUPPORTED, 
		REQUEST_MUSTBESUPPORTED
	}

	public static enum ErrorToken {
		GRANTTYPE_MUSTBENOTEMPTY, 
		CODE_MUSTBENOTEMPTY, 
		REDIRECTURI_MUSTBENOTEMPTY, 
		CLIENTID_MUSTBENOTEMPTY,
		GRANTTYPE_MUSTBESUPPORTED, 
		CLIENTID_MUSTEXISTS, 
		BASICCREDENTIALS_CLIENTREQUIRES,
		BASICCREDENTIALS_MUSTMATCHCLIENTID, 
		BASICCREDENTIALS_MUSTMATCHSECRET, 
		CODE_CLIENTID_MUSTBESTRING,
		CODE_CLIENTID_MUSTMATCHCLIENTID, 
		CODE_REDIRECTURI_MUSTBESTRING, 
		CODE_REDIRECTURI_MUSTMATCHREDIRECTURI,
		CODE_CODECHALLENGE_MUSTBESTRING, 
		CODE_CODECHALLENGEMETHOD_MUSTBESTRING, 
		CODEVERIFIER_CLIENTMUSTALLOWPKCE,
		CODE_CODECHALLENGE_CLIENTMUSTALLOWPKCE, 
		CODE_CODECHALLENGEMETHOD_CLIENTMUSTALLOWPKCE,
		CODEVERIFIER_CLIENTREQUIRESPKCE, 
		CODE_CODECHALLENGE_CLIENTREQUIRESPKCE,
		CODE_CODECHALLENGEMETHOD_CLIENTREQUIRESSTRONGPKCE, 
		CODEVERIFIER_BASE64SHA256MUSTMATCHCODECHALLENGE,
		CODEVERIFIER_MUSTMATCHCODECHALLENGE
	}

	private Persistence persistence;
	private Auditing auditing;
	private JSONWebKeyStore jsonWebKeyStore;
	private KeySelector<String> keySelector;
	private JwtValidator jwtValidator;

	public Service(Persistence persistence, Auditing auditing, URL keystoreUrl) {
		validateNotNull(persistence, ErrorNew.PERSISTENCE_MUSTBENOTNULL);
		validateNotNull(auditing, ErrorNew.AUDITING_MUSTBENOTNULL);
		this.jsonWebKeyStore = new JSONWebKeyStore(keystoreUrl);
		this.keySelector = new KeySelector<String>() { // TODO General KeySelector
			@Override
			public SelectedKey getSelectedKey(String keyId) {
				return new SelectedKey("hmac1", "oct", null, HmacKey.ALGORITHM);
			}
		};
		this.persistence = persistence;
		this.auditing = auditing;
		this.jwtValidator = new JwtValidator(this.jsonWebKeyStore.getPublicKeys());
	}

	public Object authorize(String remoteIpAddress, String xForwardedFor, String sessionToken, String clientId,
			String responseType, String redirectUri, String scope, String state, String codeChallenge,
			String codeChallengeMethod, String nonce, String display, String prompt, String maxAge, String uiLocales,
			String idTokenHint, String loginHint, String acrValues, String request) {
		return this.auditing.getOnAudit(() -> {
			// Validate authentication
			// Validations.validateNotEmpty(sessionToken,
			// ErrorAuthorize.SESSIONTOKEN_MUSTBENOTEMPTY);

			// Standard OAuth2 Errors
			// These parameters must be validate before,
			// because these errors cannot be reported back
			validateNotEmpty(clientId, ErrorAuthorize.CLIENTID_MUSTBENOTEMPTY);
			String[] authorizationScopes = scope != null ? scope.split(" +") : new String[0];
			if (redirectUri == null) {
				var hasOpenidScope = Arrays.asList(authorizationScopes).indexOf("openid") >= 0;
				validateNot(hasOpenidScope, ErrorAuthorize.REDIRECTURI_MUSTBENOTEMPTYWHENSCOPECONTAINSOPENID);
			} else {
				var redirectUriEditor = Exceptions.translate(() -> new UrlEditor(redirectUri), (e) -> new EnumException(ErrorAuthorize.REDIRECTURI_MUSTBEURL, e));
				validate(redirectUriEditor.isAbsolute(), ErrorAuthorize.REDIRECTURI_MUSTBEABSOLUTEURL, redirectUri);
				validateNot("http".equals(redirectUriEditor.getProtocol()), ErrorAuthorize.REDIRECTURI_MUSTBESECUREURL, redirectUri);
				validate(redirectUriEditor.getFragment() == null, ErrorAuthorize.REDIRECTURI_MUSTNOTHAVEFRAGMENT, redirectUri);
			}
			Client client = getOnTx(READONLY, READ_COMMITED,tx->
				Exceptions.translate(
					()->this.persistence.selectCachedMergedClientById(tx, clientId),
					byEnum(Persistence.ErrorSelectMergedClientById.CLIENTID_NOTFOUND, ErrorAuthorize.CLIENTID_MUSTEXISTS, clientId)
				)
			);
			String implicitRedirectUri;
			if(redirectUri == null) {
				validate(client.redirectUris.size() == 1, ErrorAuthorize.REDIRECTURI_MUSTBENOTEMPTYWHENCLIENTHASMULTIPLEREDIRECTURIS);
				implicitRedirectUri = client.redirectUris.get(0);
			} else {
				validate(binarySearch(client.redirectUris.toArray(String[]::new), redirectUri) >= 0, ErrorAuthorize.REDIRECTURI_MUSTBECLIENTREGISTERED, redirectUri);
				implicitRedirectUri = client.redirectUris.get(0);
			}

			//These parameters can be reported back in case of errors.
			validateNotEmpty(responseType, ErrorAuthorize.RESPONSETYPE_MUSTBENOTEMPTY, implicitRedirectUri, state);
			validate(asList(AuthorizeEndpoint.REQUEST_PARAMETER_RESPONSE_TYPE_CODE , AuthorizeEndpoint.REQUEST_PARAMETER_RESPONSE_TYPE_TOKEN).contains(responseType), ErrorAuthorize.RESPONSETYPE_MUSTBESUPPORTED, responseType, implicitRedirectUri, state);
			validate(binarySearch(client.supportedResponseTypes.toArray(String[]::new), responseType) >= 0, ErrorAuthorize.RESPONSETYPE_MUSTBECLIENTSUPPORTED, responseType, implicitRedirectUri, state);
			String[] clientScopes = client.scopes.toArray(String[]::new);
			for (String authorizationScope : authorizationScopes) {
				validate(binarySearch(clientScopes, authorizationScope) >= 0, ErrorAuthorize.SCOPE_MUSTBECLIENTSUPPORTED, authorizationScope, implicitRedirectUri, state);	
			}

			//Unsupported Parameters Features... Yet.
			validate(display == null, ErrorAuthorize.DISPLAY_MUSTBESUPPORTED, implicitRedirectUri, state);
			validate(prompt == null, ErrorAuthorize.PROMPT_MUSTBESUPPORTED, implicitRedirectUri, state);
			validate(maxAge == null, ErrorAuthorize.MAXAGE_MUSTBESUPPORTED, implicitRedirectUri, state);
			validate(uiLocales == null, ErrorAuthorize.REQUEST_MUSTBESUPPORTED, implicitRedirectUri, state);
			validate(idTokenHint == null, ErrorAuthorize.IDTOKENHINT_MUSTBESUPPORTED, implicitRedirectUri, state);
			validate(loginHint == null, ErrorAuthorize.LOGINHINT_MUSTBESUPPORTED, implicitRedirectUri, state);
			validate(acrValues == null, ErrorAuthorize.ACRVALUES_MUSTBESUPPORTED, implicitRedirectUri, state);
			validate(request == null, ErrorAuthorize.REQUEST_MUSTBESUPPORTED, implicitRedirectUri, state);
			
			//TODO Custom Validations are inserted Here

			if (responseType.equals(oauth2.AuthorizeEndpoint.REQUEST_PARAMETER_RESPONSE_TYPE_TOKEN)) {
				//return authorizeInAuthorizationImplicitGrant(); //TODO Implicit Grant
			}
			return authorizeInAuthorizationCodeGrant(client, redirectUri, implicitRedirectUri, scope, state, codeChallenge, codeChallengeMethod, nonce);
		}, App.APP_NAME, "sso.Service.authorize", remoteIpAddress, xForwardedFor, sessionToken, responseType, clientId, redirectUri, scope, state, codeChallenge, codeChallengeMethod, nonce, display, prompt, maxAge, uiLocales, idTokenHint, loginHint, acrValues, request);
	}
	
	private AuthorizationCodeGrantResponse authorizeInAuthorizationCodeGrant(Client client, String redirectUri, String implicitRedirectUri, String scope, String state, String codeChallenge, String codeChallengeMethod, String nonce) {
		//Validate all PKCE related parameters and combinations. Note that code_challenge_method parameter is OPTIONAL. 
		if (client.pkceConfig == PkceConfig.FORBIDDEN) {
			validateNull(codeChallenge, ErrorAuthorize.CODECHALLENGE_CLIENTMUSTALLOWPKCE, implicitRedirectUri, state);
			validateNull(codeChallengeMethod, ErrorAuthorize.CODECHALLENGEMETHOD_CLIENTMUSTALLOWPKCE, implicitRedirectUri, state);
		}
		if(codeChallengeMethod != null) {
			validate(
				codeChallengeMethod.equals(AuthorizeEndpoint.PKCE_REQUEST_PARAMETER_CODE_CHALLENGE_METHOD_PLAIN)
				|| codeChallengeMethod.equals(AuthorizeEndpoint.PKCE_REQUEST_PARAMETER_CODE_CHALLENGE_METHOD_S256)
				, ErrorAuthorize.CODECHALLENGEMETHOD_MUSTBESUPPORTED
				, implicitRedirectUri, state
			);
		}
		if(client.pkceConfig == PkceConfig.ENFORCED || client.pkceConfig == PkceConfig.ENFORCED_S256) {
			validateNotNull(codeChallenge, ErrorAuthorize.CODECHALLENGE_CLIENTREQUIRESPKCE, implicitRedirectUri, state);
		}
		if(client.pkceConfig == PkceConfig.ENFORCED_S256 || (client.pkceConfig == PkceConfig.OPTIONAL_S256 && codeChallenge != null)) {
			validate(AuthorizeEndpoint.PKCE_REQUEST_PARAMETER_CODE_CHALLENGE_METHOD_S256.equals(codeChallengeMethod), ErrorAuthorize.CODECHALLENGEMETHOD_CLIENTREQUIRESSTRONGPKCE, implicitRedirectUri, state);
		}
		
		//Generate authorization response code
		JwtClaims claims = new JwtClaims();
		claims.setClaim(AuthorizationCodeClaims.CLIENT_ID, client.id);
		if (redirectUri != null) {
			claims.setClaim(AuthorizationCodeClaims.REDIRECT_URI, redirectUri);
		}
		claims.setExpirationTime(NumericDate.fromMilliseconds(Instant.now().plus(Duration.ofSeconds(CODE_JWT_EXP_DURATION)).toEpochMilli()));
		if (scope != null) {
			claims.setClaim(AuthorizationCodeClaims.SCOPE, scope);
		}
		if (codeChallenge != null) {
			claims.setClaim(AuthorizationCodeClaims.CODE_CHALLENGE, codeChallenge);
		}
		if (codeChallengeMethod != null) {
			claims.setClaim(AuthorizationCodeClaims.CODE_CHALLENGE_METHOD, codeChallenge);
		}
		if (nonce != null) {
			claims.setClaim(AuthorizationCodeClaims.NONCE, nonce);
		}
		JsonWebSignature jws = new JsonWebSignature();
		JsonWebKeySet publicKeys = this.jsonWebKeyStore.getPublicKeys();
		SelectedKey selectedKey = this.keySelector.getSelectedKey(client.id);
		JsonWebKey key = publicKeys.findJsonWebKey(selectedKey.keyId, selectedKey.keyType, selectedKey.use, selectedKey.keyAlgorithm);
		validateNotNull(key, ErrorAuthorize.CODEJWTSIGNATUREKEY_MUSTBEFOUNDINKEYSTORE, implicitRedirectUri, state);
		jws.setKeyIdHeaderValue(key.getKeyId());
		jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);
		if (key instanceof PublicJsonWebKey) {
			PublicJsonWebKey publicJsonWebKey = (PublicJsonWebKey) key;
			jws.setKey(publicJsonWebKey.getPrivateKey());
		} else {
			jws.setKey(key.getKey());
		}
		jws.setPayload(claims.toJson());
		String codeJwt = Exceptions.re(() -> jws.getCompactSerialization());

		//Return to redirect_uri with code and state. 
		return new AuthorizationCodeGrantResponse(implicitRedirectUri, codeJwt, state);
	}
	
//	public String authorizeInAuthorizationImplicitGrant(String proxyIpAddress, String userIpAddress, String sessionToken, String responseType, String clientId, String redirectUri, String scope, String state) {
//		return null;
//	}
	
	public TokenResponse token(String remoteIpAddress, String xForwardedFor, UsernamePasswordCredentials basicCredentials, String grantType, String code, String redirectUri, String clientId, String codeVerifier) {
//		return this.auditing.getOnAudit(()-> {

			validateNotEmpty(grantType, ErrorToken.GRANTTYPE_MUSTBENOTEMPTY);
			validateNotEmpty(code, ErrorToken.CODE_MUSTBENOTEMPTY);
			validateNotEmpty(redirectUri, ErrorToken.REDIRECTURI_MUSTBENOTEMPTY);
			validateNotEmpty(clientId, ErrorToken.CLIENTID_MUSTBENOTEMPTY);
			
			validate(
				grantType.equals(TokenEndpoint.REQUEST_FORM_PARAMETER_GRANT_TYPE_AUTHORIZATION_CODE)
				// || grantType.equals(TokenEndpoint.REQUEST_FORM_PARAMETER_GRANT_TYPE_PASSWORD)
				|| grantType.equals(TokenEndpoint.REQUEST_FORM_PARAMETER_GRANT_TYPE_CLIENT_CREDENTIALS)
				, ErrorToken.GRANTTYPE_MUSTBESUPPORTED
			);

			Map<String, Object> codeClaims = this.jwtValidator.validate(code);

			//Validate types
			var codeClientId = validateType(codeClaims.get(AuthorizationCodeClaims.CLIENT_ID), String.class, ErrorToken.CODE_CLIENTID_MUSTBESTRING);
			validate(clientId.equals(codeClientId), ErrorToken.CODE_CLIENTID_MUSTMATCHCLIENTID);
			var codeRedirectUri = validateType(codeClaims.get(AuthorizationCodeClaims.REDIRECT_URI), String.class, ErrorToken.CODE_REDIRECTURI_MUSTBESTRING);
			validate(redirectUri.equals(codeRedirectUri), ErrorToken.CODE_REDIRECTURI_MUSTMATCHREDIRECTURI);

			Client client = getOnTx(READONLY, READ_COMMITED,tx->
				Exceptions.translate(
					()->this.persistence.selectCachedMergedClientById(tx, clientId),
					byEnum(Persistence.ErrorSelectMergedClientById.CLIENTID_NOTFOUND, ErrorToken.CLIENTID_MUSTEXISTS)
				)
			);

			//Validate Authentication
			if (client.tokenEndpointAuthType == TokenEndpointAuthType.SECRET_BASIC) {
				validateNotNull(basicCredentials, ErrorToken.BASICCREDENTIALS_CLIENTREQUIRES);
				validate(clientId.equals(basicCredentials.username), ErrorToken.BASICCREDENTIALS_MUSTMATCHCLIENTID);
				validate(client.secret.equals(basicCredentials.password), ErrorToken.BASICCREDENTIALS_MUSTMATCHSECRET);
			}

			//PKCE Related validations.
			var codeCodeChallenge = validateType(codeClaims.get(AuthorizationCodeClaims.CODE_CHALLENGE), String.class, ErrorToken.CODE_CODECHALLENGE_MUSTBESTRING);
			var codeCodeChallengeMethod = validateType(codeClaims.get(AuthorizationCodeClaims.CODE_CHALLENGE_METHOD), String.class, ErrorToken.CODE_CODECHALLENGEMETHOD_MUSTBESTRING);
			if (client.pkceConfig == PkceConfig.FORBIDDEN) {
				validateNull(codeVerifier, ErrorToken.CODEVERIFIER_CLIENTMUSTALLOWPKCE);
				validateNull(codeCodeChallenge, ErrorToken.CODE_CODECHALLENGE_CLIENTMUSTALLOWPKCE);
				validateNull(codeCodeChallengeMethod, ErrorToken.CODE_CODECHALLENGEMETHOD_CLIENTMUSTALLOWPKCE);
			}
			if(codeCodeChallengeMethod != null) {
				validate(
					asList(
						AuthorizeEndpoint.PKCE_REQUEST_PARAMETER_CODE_CHALLENGE_METHOD_PLAIN, 
						AuthorizeEndpoint.PKCE_REQUEST_PARAMETER_CODE_CHALLENGE_METHOD_S256
					).contains(codeCodeChallengeMethod), 
					ErrorAuthorize.CODECHALLENGEMETHOD_MUSTBESUPPORTED
				);
			}
			if(client.pkceConfig == PkceConfig.ENFORCED || client.pkceConfig == PkceConfig.ENFORCED_S256) {
				validateNotNull(codeVerifier, ErrorToken.CODEVERIFIER_CLIENTREQUIRESPKCE);
				validateNotNull(codeCodeChallenge, ErrorToken.CODE_CODECHALLENGE_CLIENTREQUIRESPKCE);
			}
			if(client.pkceConfig == PkceConfig.ENFORCED_S256 || (client.pkceConfig == PkceConfig.OPTIONAL_S256 && codeCodeChallenge != null)) {
				validate(AuthorizeEndpoint.PKCE_REQUEST_PARAMETER_CODE_CHALLENGE_METHOD_S256.equals(codeCodeChallengeMethod), ErrorToken.CODE_CODECHALLENGEMETHOD_CLIENTREQUIRESSTRONGPKCE);
			}
			if(codeCodeChallenge != null) {
				if (AuthorizeEndpoint.PKCE_REQUEST_PARAMETER_CODE_CHALLENGE_METHOD_S256.equals(codeCodeChallengeMethod)) {
					MessageDigest sha256Digest = re(()->MessageDigest.getInstance("SHA-256"));
					var codeVerifierSha256Bytes = sha256Digest.digest(codeVerifier.getBytes(Charset.forName("US-ASCII")));
					var codeVerifierSha256UrlEncoded = Base64.getUrlEncoder().encodeToString(codeVerifierSha256Bytes);
					validate(codeVerifierSha256UrlEncoded.equals(codeCodeChallenge), ErrorToken.CODEVERIFIER_BASE64SHA256MUSTMATCHCODECHALLENGE) ;
				} else {
					validate(codeVerifier.equals(codeCodeChallenge), ErrorToken.CODEVERIFIER_MUSTMATCHCODECHALLENGE) ;
				}
			}

			return new TokenResponse();
//		}, App.APP_NAME, "sso.Service.token", proxyIpAddress, userIpAddress, basicCredentials, grantType, code, redirectUri, clientId, codeVerifier);

	}
	
	
	// public static void main(String[] args) {
	// 	OctetSequenceJsonWebKey x = new OctetSequenceJsonWebKey(new HmacKey("12345678901234567890123456789012".getBytes()));
	// 	x.setKeyId("hmac1");
	// 	x.setAlgorithm(HmacKey.ALGORITHM);
    //     final JsonWebKeySet jsonWebKeySet = new JsonWebKeySet(x);
    //     final String data = jsonWebKeySet.toJson(JsonWebKey.OutputControlLevel.INCLUDE_PRIVATE);
    //     System.out.println(data);
	// }
}
