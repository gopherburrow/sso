package sso.entity;

public class UsernamePasswordCredentials {
	public String username;
	public String password;
	
	public UsernamePasswordCredentials(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
}
