package sso.entity;

import java.util.ArrayList;
import java.util.List;

public class Client {
	public static enum PkceConfig {
		FORBIDDEN,
		OPTIONAL,
		OPTIONAL_S256,
		ENFORCED,
		ENFORCED_S256
	}

	public static enum TokenEndpointAuthType {
		NONE,
		SECRET_BASIC
	}

	public String id;
	public String name;
	public String secret;
	public PkceConfig pkceConfig;
	public TokenEndpointAuthType tokenEndpointAuthType;
	public List<String> supportedResponseTypes;
	public List<String> redirectUris;
	public List<String> scopes;

	public Client(String id, String name, PkceConfig pkceConfig, TokenEndpointAuthType tokenEndpointAuthType, String secret) {
		super();
		this.id = id;
		this.name = name;
		this.pkceConfig = pkceConfig;
		this.tokenEndpointAuthType = tokenEndpointAuthType;
		this.secret = secret;
		this.supportedResponseTypes = new ArrayList<>();
		this.redirectUris = new ArrayList<>();
		this.scopes = new ArrayList<>();
	}
}