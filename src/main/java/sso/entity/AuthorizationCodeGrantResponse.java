package sso.entity;

public class AuthorizationCodeGrantResponse {
	public String redirect_uri;
	public String code;
	public String state;
	public AuthorizationCodeGrantResponse(String redirect_uri, String code, String state) {
		super();
		this.redirect_uri = redirect_uri;
		this.code = code;
		this.state = state;
	}
}
