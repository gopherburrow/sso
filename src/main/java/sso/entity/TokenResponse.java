package sso.entity;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TokenResponse {
	@JsonProperty("access_token")
	public String accessToken;
	
	@JsonProperty("tokenType")
	public String tokenType;

	@JsonProperty("expires_in")
	@JsonInclude(Include.NON_NULL)
	public Instant expiresIn;

	@JsonProperty("refresh_token")
	@JsonInclude(Include.NON_NULL)
	public String refreshToken;

	@JsonProperty("scope")
	@JsonInclude(Include.NON_NULL)
	public String scope;

	@JsonProperty("id_token")
	public String idToken;
}
