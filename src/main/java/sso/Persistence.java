package sso;

import static freud.err.ExceptionTranslation.byEnum;
import static freud.err.Exceptions.re;
import static freud.sql.Sql.find;
import static freud.sql.Sql.findList;
import static freud.sql.merger.Merger.keys;
import static freud.tx.sql.SqlTx.getConnection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.time.Duration;
import java.util.List;

import javax.sql.DataSource;

import freud.cache.Cache;
import freud.err.Exceptions;
import freud.sql.Param;
import freud.sql.Sql;
import freud.sql.Sql.ErrorFind;
import freud.sql.merger.KeyedEntity;
import freud.sql.merger.Merger;
import freud.tx.Transaction;
import io.micrometer.core.instrument.MeterRegistry;
import microsvc.monitoring.TimeMonitoring;
import sso.entity.Client;
import sso.entity.Client.PkceConfig;
import sso.entity.Client.TokenEndpointAuthType;

public class Persistence {
	
	public static enum ErrorSelectMergedClientById {
		CLIENTID_NOTFOUND
	}

	private static final String SQL_DDL = 
		"CREATE TYPE Pkce_Config_Type AS ENUM (  \n" +
		"	'FORBIDDEN', \n" +
		"	'OPTIONAL', \n" +
		"	'OPTIONAL_S256', \n" +
		"	'ENFORCED', \n" +
		"	'ENFORCED_S256' \n" +
		"); \n" +
		"\n" +
		"CREATE TYPE Token_Endpoint_Auth_Type AS ENUM ( \n" +
		"	'NONE', \n" +
		"	'SECRET_BASIC' \n" +
		"); \n" +
		"\n" +
		"CREATE TABLE Client ( \n" +
		"	id VARCHAR(255) NOT NULL, \n" +
		"	name VARCHAR(255) NOT NULL, \n" +
		"	pkce_config Pkce_Config_Type NOT NULL, \n" +
		"	token_endpoint_auth_type Token_Endpoint_Auth_Type NOT NULL, \n" +
		"	secret VARCHAR(255), \n" +
		"	CONSTRAINT Client_Pk PRIMARY KEY (id) \n" +
		"); \n" +
		"\n" +
		"CREATE TYPE Response_Type_Type AS ENUM ( \n" +
		"	'code', \n" +
		"	'token' \n" +
		"); \n" +
		"\n" +
		"CREATE TABLE Client_Supported_Response_Type ( \n" +
		"	client_id VARCHAR(255) NOT NULL, \n" +
		"	response_type Response_Type_Type NOT NULL, \n" +
		"	CONSTRAINT Client_Supported_Response_Type_Pk PRIMARY KEY (client_id, response_type), \n" +
		"	CONSTRAINT Client_Supported_Response_Type_client_id_Fk FOREIGN KEY (client_id) REFERENCES Client (id) \n" +
		"); \n" +
		"\n" +
		"CREATE TABLE Client_Redirect_Uri ( \n" +
		"	client_id VARCHAR(255) NOT NULL, \n" +
		"	redirect_uri VARCHAR(255) NOT NULL, \n" +
		"	CONSTRAINT Client_Redirect_Uri_Pk PRIMARY KEY (client_id, redirect_uri), \n" +
		"	CONSTRAINT Client_Redirect_Uri_client_id_Fk FOREIGN KEY (client_id) REFERENCES Client (id) \n" +
		"); \n" +
		"\n" +
		"CREATE TABLE Client_Scope ( \n" +
		"	client_id VARCHAR(255) NOT NULL, \n" +
		"	scope VARCHAR(255) NOT NULL, \n" +
		"	CONSTRAINT Client_Scope_Pk PRIMARY KEY (client_id, scope), \n" +
		"	CONSTRAINT Client_Scope_client_id_Fk FOREIGN KEY (client_id) REFERENCES Client (id) \n" +
		"); \n";

	private static final String SQL_SELECT_CLIENT_BY_ID = 
		"SELECT \n" +
		"	Client.id, \n" +
		"	Client.name, \n" +
		"	Client.pkce_config, \n" +
		"	Client.token_endpoint_auth_type, \n" +
		"	Client.secret \n" +
		"FROM \n" +
		"	Client \n" +
		"WHERE \n" +
		"	Client.id = ? \n" +
		"ORDER BY \n" +
		"	Client.id \n";

	private static final String SQL_SELECT_CLIENT_SUPPORTED_RESPONSE_TYPES_BY_CLIENT_ID = 
		"SELECT \n" +
		"	Client_Supported_Response_Type.client_id, \n" +
		"	Client_Supported_Response_Type.response_type \n" +
		"FROM \n" +
		"	Client_Supported_Response_Type \n" +
		"WHERE \n" +
		"	Client_Supported_Response_Type.client_id = ? \n" +
		"ORDER BY \n" +
		"	Client_Supported_Response_Type.client_id, \n" +
		"	Client_Supported_Response_Type.response_type \n";

	private static final String SQL_SELECT_CLIENT_REDIRECT_URIS_BY_CLIENT_ID = 
		"SELECT \n" +
		"	Client_Redirect_Uri.client_id, \n" +
		"	Client_Redirect_Uri.redirect_uri \n" +
		"FROM \n" +
		"	Client_Redirect_Uri \n" +
		"WHERE \n" +
		"	Client_Redirect_Uri.client_id = ? \n" +
		"ORDER BY \n" +
		"	Client_Redirect_Uri.client_id, \n" +
		"	Client_Redirect_Uri.redirect_uri \n";

	private static final String SQL_SELECT_CLIENT_SCOPES_BY_CLIENT_ID = 
		"SELECT \n" +
		"	Client_Scope.client_id, \n" +
		"	Client_Scope.scope \n" +
		"FROM \n" +
		"	Client_Scope \n" +
		"WHERE \n" +
		"	Client_Scope.client_id = ? \n" +
		"ORDER BY \n" +
		"	Client_Scope.client_id, \n" +
		"	Client_Scope.scope \n";

	private DataSource dataSource;
	private TimeMonitoring monitoring;
	
	private Cache<Client, String> clientCache;

	public Persistence(DataSource dataSource, MeterRegistry meterRegistry, long cacheExpirationInSeconds) {
		this.dataSource = dataSource;
        this.monitoring = new TimeMonitoring(meterRegistry);
        this.clientCache = new Cache<>(Duration.ofSeconds(cacheExpirationInSeconds));
	}
	
	public Client selectCachedMergedClientById(Transaction tx, String clientId) {
		return this.clientCache.get(keys->selectMergedClientById(tx, keys[0]), clientId);
	}

	public Client selectMergedClientById(Transaction tx, String clientId) {
		var client = Exceptions.translate(()->selectClientById(tx, clientId), 
			byEnum(ErrorFind.SQL_MUSTFOUNDARECORD, ErrorSelectMergedClientById.CLIENTID_NOTFOUND)
		);
		var responseTypes = selectClientSupportedResponseTypesByClientId(tx, clientId);
		var redirectUris = selectClientRedirectUrisByClientId(tx, clientId);
		var scopes = selectClientScopesByClientId(tx, clientId);

		Merger.join(client, c->keys(c.id), responseTypes, ke->ke.getKeys(), (c, k, r)->c.supportedResponseTypes.add(r.getEntity()));
		Merger.join(client, c->keys(c.id), redirectUris, ke->ke.getKeys(), (c, k, r)->c.redirectUris.add(r.getEntity()));
		Merger.join(client, c->keys(c.id), scopes, ke->ke.getKeys(), (c, k, r)->c.scopes.add(r.getEntity()));

		return client;
	}

	
	public Client selectClientById(Transaction tx, String id) {
    	return this.monitoring.getOn(()->
			re(()->find(
				getConnection(this.dataSource, tx), 
				SQL_SELECT_CLIENT_BY_ID, 
				this::getClient,
				new Param(1, Types.VARCHAR, id) 
			)),
			"op", "sso.Persistence.selectClientById"
    	);
	}

	public List<KeyedEntity<String, String>> selectClientSupportedResponseTypesByClientId(Transaction tx, String clientId) {
    	return this.monitoring.getOn(()->
			re(()->findList(
				getConnection(this.dataSource, tx), 
				SQL_SELECT_CLIENT_SUPPORTED_RESPONSE_TYPES_BY_CLIENT_ID, 
				rs->{ 
					var c = rs.getString("client_id");
					var responseType = rs.getString("response_type");
					return new KeyedEntity<String, String>(responseType, keys(c, responseType)); 
				},
				new Param(1, Types.VARCHAR, clientId) 
			)),
			"op", "sso.Persistence.selectClientSupportedResponseTypesByClientId"
    	);
	}

	public List<KeyedEntity<String, String>> selectClientRedirectUrisByClientId(Transaction tx, String clientId) {
    	return this.monitoring.getOn(()->
			re(()->findList(
				getConnection(this.dataSource, tx), 
				SQL_SELECT_CLIENT_REDIRECT_URIS_BY_CLIENT_ID, 
				rs->{ 
					var c = rs.getString("client_id");
					var redirectUri = rs.getString("redirect_uri");
					return new KeyedEntity<String, String>(redirectUri, keys(c, redirectUri)); 
				},
				new Param(1, Types.VARCHAR, clientId) 
			)),
			"op", "sso.Persistence.selectClientRedirectUrisByClientId"
    	);
	}

	public List<KeyedEntity<String, String>> selectClientScopesByClientId(Transaction tx, String clientId) {
    	return this.monitoring.getOn(()->
			re(()->findList(
				getConnection(this.dataSource, tx), 
				SQL_SELECT_CLIENT_SCOPES_BY_CLIENT_ID, 
				rs->{ 
					var c = rs.getString("client_id");
					var redirectUri = rs.getString("scope");
					return new KeyedEntity<String, String>(redirectUri, keys(c, redirectUri)); 
				},
				new Param(1, Types.VARCHAR, clientId) 
			)),
			"op", "sso.Persistence.selectClientScopesByClientId"
    	);
	}


	private Client getClient(ResultSet rs) throws SQLException {
		return new Client(
			rs.getString("id"),
			rs.getString("name"),
			Sql.getEnumFromName(PkceConfig.class, rs, "pkce_config"),
			Sql.getEnumFromName(TokenEndpointAuthType.class, rs, "token_endpoint_auth_type"),
			rs.getString("secret")
		);
	}
}
