package sso;

public class SelectedKey {
	public String keyId;
	public String keyType;
	public String use;
	public String keyAlgorithm;
	
	public SelectedKey(String keyId, String keyType, String use, String keyAlgorithm) {
		super();
		this.keyId = keyId;
		this.keyType = keyType;
		this.use = use;
		this.keyAlgorithm = keyAlgorithm;
	}
}
